# Driver Adaptability Model

Code and model data for Hautaoja (2025).

Predicting Driver Adaptability: A Computational Rationality Model for Attention Allocation In Lateral Vehicle Control.

Code based on: Jokinen, Kujala, & Oulasvirta (2020). Multitasking in Driving As Optimal Adaptation under Uncertainty. Human Factors.


HOW TO RUN AND TRAIN NEW MODEL: 

1. Install all the relevant packages (preferably in venv)
	scikit_image
	matplotlib
	stable_baselines3
	gym
	AND POSSIBLE OTHERS !!!

2. Navigate to the driving_agent.py 

3. In the end of the file, there is the actual executable part of 
   the code. Search for (Ctrl+F) parts of the code with "#CHANGE THIS"
   and change 1. d_env.steer_noise, 2. save_dir, 3. save_path.

4. Don't add breakpoints for the #SIMULATION -part of the code.

5. Run the code and wait until the plotted trace appears. Close this with [X]
   and after that the animation should appear. After closing this with [X], 
   the model will be saved in the specified dir & path specified in 2. and 3.


HOW TO RUN TRAINED MODEL: 

1. Navigate to the load_saved_model.py

2. Search for (Ctrl+F) parts of the code with "#CHANGE THIS" and
   change 1. d_env.steer_noise, 2. speed (a or b), 3. model_path.

3. Run the code and wait until the plotted trace appears. Close this with [X]
   and after that the animation should appear. Close this with [X].

NOTE: It's recommended to train 3-4 different agents, because the reliability of the current model is not yet perfect.
      The full training of 3-4 agents with 60/80/100 speeds can take anywhere between 1-2 hours, so please be patient! :-)