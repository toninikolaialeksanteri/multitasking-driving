#!/bin/bash -l
#SBATCH --time=02:00:00
#SBATCH --mem-per-cpu=50M
#SBATCH --output=adas-out/%a.out
#SBATCH --cpus-per-task=22
#SBATCH --open-mode=append
#SBATCH --array=1-2

module load puck_python
n=$SLURM_ARRAY_TASK_ID
iteration=`sed -n "${n} p" adas_params.txt`
srun python3 run_adas.py ${iteration}
