import numpy as np
import matplotlib
from matplotlib import pyplot as plt
from matplotlib.patches import Rectangle, Wedge
from matplotlib import animation

# THE CENTER OF THE CAR IS A BIT OFF, BUT THE ANIMATION GIVES A ROUGH ESTIMATE.
def animate_trace(env, trace):
    plt.close()
    interval = env.dt
    frames = len(env.trace)

    fig = plt.figure(figsize=(8, 8))
    ax = fig.add_subplot(1, 1, 1, aspect='equal')
    ax.set_xlim(0, env.area_bounds)
    ax.set_ylim(0, env.area_bounds)

    axl = plt.gca()
    axl.invert_yaxis()

    # Car dimensions (rectangle) in the new scale
    car_length = 2.7  # Corresponds to 2.7 meters
    car_width = 1.5   # Corresponds to 1.5 meters
    car = Rectangle((0, 0), car_length, car_width, fc='g', angle=0)

    # Add road visualization
    road_center_x = env.area_bounds / 2
    road_center_y = env.area_bounds / 2
    outer_radius = env.track_radius + env.track_width / 2  # Outer edge of the road
    inner_radius = env.track_radius - env.track_width / 2  # Inner edge of the road

    road_outer = Wedge(
        (road_center_x, road_center_y),
        outer_radius,
        0,
        360,
        width=env.track_width,
        color="gray",
        alpha=0.5,
    )
    ax.add_patch(road_outer)

    # Add environment area visualization
    area = plt.imshow(env.area.transpose(), norm=matplotlib.colors.Normalize(vmin=0, vmax=20), cmap='binary', alpha=0.3)
    ax.add_artist(car)

    # Adjust text positions to fit the new scaling and resolution
    text_offset = 100
    time_text = ax.text(text_offset, text_offset * 1, "Time:", fontsize=8)
    speed_text = ax.text(text_offset, text_offset * 1.5, "Speed:", fontsize=8)
    heading_text = ax.text(text_offset, text_offset * 2, "Head:", fontsize=8)
    steer_text = ax.text(text_offset, text_offset * 2.5, "Steer:", fontsize=8)
    area_text = ax.text(text_offset, text_offset * 3, "Area:", fontsize=8)
    tlc_text = ax.text(text_offset, text_offset * 3.5, "TLC:", fontsize=8)
    offset_text = ax.text(text_offset, text_offset * 4, "OFF:", fontsize=8)

    def init():
        time_text.set_text('')
        speed_text.set_text('')
        steer_text.set_text('')
        tlc_text.set_text('')
        offset_text.set_text('')
        heading_text.set_text('')
        area_text.set_text('')
        return time_text, speed_text, steer_text, heading_text, area_text, tlc_text, offset_text

    def animate(i):
        if i == 0:
            ax.add_patch(car)

        t = trace['time'][i]
        t = i * interval
        speed = trace['speed'][i]
        steer = trace['steer'][i]
        tlc = trace['tlc'][i]
        offset = trace['offset'][i]
        current_area = trace['current_area'][i]
        heading = trace['heading'][i]
        x = trace['pos'][i][0]
        y = trace['pos'][i][1]

        # Update the car's position and angle
        car.set_xy((x - car_length / 2, y - car_width / 2))  # Set bottom-left corner
        car.angle = np.degrees(heading)  # Convert heading (radians) to degrees for rotation

        # Update text
        time_text.set_text(f'Time {round(t, 2):1.2f}')
        speed_text.set_text(f'Speed {round(speed * 3.6, 2):1.2f}')
        steer_text.set_text(f'Steer {round(steer, 2):1.2f}')
        tlc_text.set_text(f'TLC {round(tlc, 2):1.2f}')
        offset_text.set_text(f'OFFSET {round(offset, 2):1.2f}')
        area_text.set_text(f'Area {round(current_area, 2):1.2f}')
        heading_text.set_text(f'Heading {round(heading, 2):1.2f}')

        if trace['has_attention'][i]:
            car.set_color('g')  # Green for attention
        else:
            car.set_color('r')  # Red for no attention

        return car, time_text, speed_text, steer_text, heading_text, area_text, tlc_text, offset_text

    anim = animation.FuncAnimation(
        fig,
        animate,
        init_func=init,
        frames=frames,
        interval=1000 * interval,
        blit=True,
    )

    plt.show()




# ## OLD CODE
# import numpy as np
# import matplotlib
# from matplotlib import pyplot as plt
# from matplotlib import animation

# # Animates the plotted trace of the driving agent.
# def animate_trace(env, trace):
#     plt.close()
#     interval = env.dt

#     frames = len(env.trace)

#     fig = plt.figure()
#     ax = fig.add_subplot(1, 1, 1, aspect='equal')
#     ax.set_xlim(0, env.area_bounds)
#     ax.set_ylim(0, env.area_bounds)


#     axl = plt.gca()
#     axl.invert_yaxis()

#     car = plt.Circle((0, 0), 1, fc='g')
#     area = plt.imshow(env.area.transpose(), norm = matplotlib.colors.Normalize(vmin = 0, vmax=20), cmap='binary')

#     ax.add_artist(area)
#     time_text = ax.text(10, 75, "Time:", fontsize=8)
#     speed_text = ax.text(10, 175, "Speed:", fontsize=8)
#     heading_text = ax.text(10, 275, "Head:", fontsize=8)
#     steer_text = ax.text(10, 375, "Steer:", fontsize=8)
#     area_text = ax.text(10, 475, "Area:", fontsize=8)
#     tlc_text = ax.text(10, 575, "TLC:", fontsize=8)
#     offset_text = ax.text(10, 675, "OFF:", fontsize=8)
#     #tlcl_text = ax.text(10, 775, "TLCL:", fontsize=8)
#     #tlcr_text = ax.text(10, 875, "TLCR:", fontsize=8)


#     def init():
#         time_text.set_text('')
#         speed_text.set_text('')
#         steer_text.set_text('')
#         tlc_text.set_text('')
#         #tlcl_text.set_text('')
#         #tlcr_text.set_text('')
#         offset_text.set_text('')
#         heading_text.set_text('')
#         area_text.set_text('')
#         return time_text, speed_text, steer_text, heading_text, area_text, tlc_text, offset_text#,tlcl_text, tlcr_text

#     def animate(i):
#         if i == 0:
#             ax.add_patch(car)
#         t = trace['time'][i]
#         t = i * interval
#         speed = trace['speed'][i]
#         steer = trace['steer'][i]
#         tlc = trace['tlc'][i]
#         #tlc_l = trace['tlc_l'][i]
#         #tlc_r = trace['tlc_r'][i]
#         offset = trace['offset'][i]
#         current_area = trace['current_area'][i]
#         heading = trace['heading'][i]
#         x = int(trace['pos'][i][0]) # pos[0] => x-coordinate
#         y = int(trace['pos'][i][1]) # pos[1] => y-coordinate
#         time_text.set_text('Time {:1.2f}'.format(round(t,2)))
#         speed_text.set_text('Speed {:1.2f}'.format(round(speed,2)))
#         steer_text.set_text('Steer {:1.2f}'.format(round(steer,2)))
#         tlc_text.set_text('TLC {:1.2f}'.format(round(tlc,2)))
#         #tlcl_text.set_text('TLC_L {:1.2f}'.format(round(tlc_l,2)))
#         #tlcr_text.set_text('TLC_R {:1.2f}'.format(round(tlc_r,2)))
#         offset_text.set_text('OFFSET {:1.2f}'.format(round(offset,2)))
#         area_text.set_text('Area {:1.2f}'.format(round(current_area,2)))
#         heading_text.set_text('Heading {:1.2f}'.format(round(heading,2)))
#         if trace['has_attention'][i]:
#             car.set_color('g') # if attention, the car is green
#         else:
#             car.set_color('r') # if not attention, the car is red
#         car.center = (x, y)
#         return car, time_text, speed_text, steer_text, heading_text, area_text, tlc_text, offset_text#, tlcl_text, tlcr_text

#     anim = animation.FuncAnimation(
#         fig,
#         animate,
#         init_func=init,
#         frames=frames,
#         interval=1000 * interval,
#         blit=True,
#     )

#     plt.show() # Breakpoint here to get the animation while debugging.


