from gym import Env
from gym.spaces import Discrete, Dict, Box
import numpy as np

import driving_environment
import driving_internal
import map_generator
import animate_trace
import random
import time
import os
import csv

import copy

import importlib
importlib.reload(driving_environment)
importlib.reload(driving_internal)
importlib.reload(map_generator)
importlib.reload(animate_trace)

from stable_baselines3.common.callbacks import BaseCallback

from stable_baselines3 import PPO
#from stable_baselines3 import SAC

class SaveBestModelCallback(BaseCallback): # @BEST
    """
    Callback for saving the best model based on episode rewards.
    """
    def __init__(self, save_path, verbose=1):
        super(SaveBestModelCallback, self).__init__(verbose)
        self.save_path = save_path
        self.best_mean_reward = -float('inf')

    def _on_step(self):
        # Retrieve performance metric and update if best
        if len(self.model.ep_info_buffer) > 0 and self.model.ep_info_buffer[-1]['r'] > self.best_mean_reward:
            self.best_mean_reward = self.model.ep_info_buffer[-1]['r']
            self.model.save(os.path.join(self.save_path, 'best_model_X.zip')) ### CHANGE 1/2 e.g. "best_model1, best_model2..etc" IF MULTIPLE SIMULTANEOUS RUNS! SAVES HERE. 
            if self.verbose:
                print(f"!!!!!!!!!!!!!!!!!!!!  NEW BEST MODEL POLICY SAVED WITH MEAN REWARD OF: {self.best_mean_reward:.2f}  !!!!!!!!!!!!!!!!!!!!")
        return True

# -------------- FOR RANDOM SPEED CHANGE AT ROLLOUT --------------
class RandomSpeedCallback(BaseCallback):
    """
    At the start of each rollout (i.e., each new episode or batch),
    pick a random speed in [60 km/h, 100 km/h], and set env.d_env.min_speed
    and env.d_env.max_speed accordingly.
    """
    def __init__(self, env, min_kmh=60, max_kmh=100, verbose=0):
        super(RandomSpeedCallback, self).__init__(verbose)
        self.env = env
        self.min_speed_ms = min_kmh / 3.6
        self.max_speed_ms = max_kmh / 3.6

    def _on_rollout_start(self):
        # Pick a random speed in [min_speed_ms, max_speed_ms]
        speed_ms = np.random.uniform(self.min_speed_ms, self.max_speed_ms)
        # Give a small +/- margin so the agent doesn’t “lock” to a single speed
        self.env.ext_env.min_speed = speed_ms - 0.5
        self.env.ext_env.max_speed = speed_ms + 0.5

        if self.verbose > 0:
            print(f"[RandomSpeedCallback] Setting speed to ~{3.6*speed_ms:.2f} km/h")

    def _on_step(self) -> bool:
        # Simply return True so the training continues
        return True


# PART 3/3: THE AGENT
# Implementation of an actual agent usin PPO to learn optimal policy for the internal and external environments. 
class driving_agent(Env):
    def __init__(self, int_env, params = None, csv_file="training_log1.csv"):

        # Initializes the internal and external environments.
        self.int_env = int_env
        self.ext_env = int_env.ext_env

        # Määrittele CSV-tiedoston polku
        self.csv_file = csv_file # MUOKKAA TÄÄLTÄ CSV-FILEN NIMEÄ

        # Check if the CSV file exists; if not, create it and add headers
        if not os.path.exists(self.csv_file):
            with open(self.csv_file, mode='w', newline='') as file:
                writer = csv.writer(file)
                writer.writerow(["Episode", "Reward", "Avg_TLC", "Speed", "Max_OOB_Time", "Done", "Duration", "Avg_Occlusion", "Phase"])

        # Defines the observation space, which is a dictionary containing various continuous state variables.
        self.observation_space = Dict(
            spaces = {
                # 'obs': Box(0,1, (2*self.int_env.sight_span+1,
                #                  2*self.int_env.sight_span+1), dtype = float),
                'tlc': Box(0, 1, (1,), dtype = float),
                #'pos': Box(0, 1, (2,), dtype = float),
                'offset': Box(-1, 1, (1,), dtype = float),
                'inattention': Box(0, 1, (1,), dtype = float),
                'speed': Box(0, 1, (1,), dtype = float),
                'heading': Box(-1, 1, (1,), dtype = float),
                'steer': Box(-1, 1, (1,), dtype = float)
            })

        # Defines the action space as a continuous 3D space for acceleration, steering and attention. Values from -1 to 1, eg. a = [0.0, -1.0 , 1.0] BUT ths depends on the max_XXX
        self.action_space = Box(low=-1, high=1, shape = (3,))

        # Initialize the rewards list and episode counter
        self.episode_rewards = []
        self.episode_counter = 0
        self.cumulative_reward = 0  # Initialize cumulative reward for the current episode
        self.total_episode_multiplier = 0 # Aid to get the total episode count

        # Muuttujat episodin okkluusioaikaa varten
        self.occlusion_times = []  # Kertynyt okkluusioaika episodin aikana
        self.current_occlusion_time = 0  # Yksittäisen timestepin aikana kertyvä okkluusioaika

        self.learning_phase = "" # The phase of training (I-XIII)
        self.simulate_flag = False # Flag to eliminate unneccessary print statements when simulating 

############# DYNAMIC LEARNING RATE ##################
        
        # Create dynamic learning rate lambda function
        # This increases the learning rate linearly from `0.00005` to `0.0003`
        # Progresses throughout each learning phase and resets always when the new one starts 
        def dynamic_lr_increase(progress: float):
            return max(0.00005, 0.0003 * (1 - progress))
        
######################################################

        self.agent = PPO("MultiInputPolicy", self, verbose = 1, # gives fps output
                    #n_steps = 1024, # 2048 default, 1024 for more frequent but smaller updates
                    #batch_size = 64, # 64 default, 32 for smaller gradients
                    #n_epochs=10, # 10 default, lower if overfitting happens

                    
                    learning_rate = 0.0004, #dynamic_lr_increase, # 0.0001 - 0.0005 (WAS 0.0004) controls the size of the updates to the policy and value function parameters during optimization
                                            # lower values = slower convergence, less instability in learning; higher values = faster convergence, more instability in learning
                    clip_range = 0.15, # 0.0 - 0.3 limits the ratio of the probabilities of taking an action under the current policy to the probability under the previous policy
                                      # If agent is converging too quickly to suboptimal policies, consider increasing; if agent seems to explore too much without improving, consider decreasing.
                    ent_coef=0.01 # 0.0 - 0.03 higher value encourages more exploration by the policy, lower reduces the emphasis on exploration in favor of exploiting known rewarding actions.
                                  # If agent is converging too quickly to suboptimal policies, consider increasing; if agent seems to explore too much without improving, consider decreasing it.
                    #gamma=0.95
                    )
######################################################

        # Use the custom callback in the train_curriculum method @BEST
        self.checkpoint_dir = "./checkpoints/"
        if not os.path.exists(self.checkpoint_dir):
            os.makedirs(self.checkpoint_dir)

        self.BEST = False # Use the best policy for the trained model @BEST

        self.reset_trace()

        self.reset()

    # Calls the reset function in the internal environment.
    def reset(self):
        self.int_env.reset()

        return self.int_env.reset()
    
    # Initializes the trace dictionary with empty lists for each variable.
    def reset_trace(self):
         self.trace = {"time": [],
                       "pos": [],
                       "speed": [],
                       "heading": [],
                       "steer": [],
                       "tlc": [],
                       "offset": [],
                       "has_attention": [],
                       "current_area": []}

    # Appends the current values of variables to their respective lists in the trace dictionary. THESE VALUES WILL BE PRINTED.
    def log_trace(self):
        self.trace['time'].append(self.ext_env.time)
        self.trace['pos'].append(copy.deepcopy(self.ext_env.pos))
        self.trace['speed'].append(self.ext_env.speed)
        self.trace['heading'].append(self.ext_env.heading)
        self.trace['steer'].append(self.ext_env.steer)
        self.trace['tlc'].append(self.ext_env.tlc) ## TLC UPDATE
        self.trace['offset'].append(self.ext_env.offset)
        self.trace['has_attention'].append(self.int_env.has_attention)
        self.trace['current_area'].append(self.ext_env.current_area()) # On lane (1) or not (2)

    # Sets an initial maximum OOB time and trains the agent accordingly without occlusions, easing the OOB time each learning phase.
    # When fully trained with 60 km/h, increases the speed to 80, then 100 km/h. Then does the same process with occlusions included.
    def train_curriculum(self):
       
       # Custom callback to save the best model based on episode rewards
       #save_best_model_callback = SaveBestModelCallback(save_path=self.checkpoint_dir) # @BEST

        start = time.time()  # Start measuring time
        self.int_env.occlusion = False  # Start with occlusion off
        # self.ext_env.use_random_speed = False # To use random speed

        # ---------------- PHASE I ---------------- #
        d_env.max_speed = 16.666 * self.ext_env.resolution_scale  # ~50km/h
        d_env.min_speed = 16.666 * self.ext_env.resolution_scale

        self.ext_env.max_oob_time = 0.1
        self.learning_phase = "I"
        self.agent.learn(total_timesteps=50000)
        print(f"Finished learning phase I (60km/h) with max OOB time of: {self.ext_env.max_oob_time}")

        # ---------------- PHASE II ---------------- #
        self.ext_env.max_oob_time = 0.5
        self.learning_phase = "II"
        self.agent.learn(total_timesteps=50000)
        print(f"Finished learning phase II (60km/h) with max OOB time of: {self.ext_env.max_oob_time}")

        # ---------------- PHASE III ---------------- #
        self.ext_env.max_oob_time = 5
        self.learning_phase = "III"
        self.agent.learn(total_timesteps=200000)
        print(f"Finished learning phase III (60km/h) with max OOB time of: {self.ext_env.max_oob_time}")

        # ---------------- PHASE IV ---------------- #
        d_env.max_speed = 22.222 * self.ext_env.resolution_scale  # ~80km/h
        d_env.min_speed = 22.222 * self.ext_env.resolution_scale

        self.ext_env.max_oob_time = 4
        self.learning_phase = "IV"
        self.agent.learn(total_timesteps=300000)
        print(f"Finished learning phase IV (80km/h) with max OOB time of: {self.ext_env.max_oob_time}")

        # ---------------- PHASE V ---------------- #
        d_env.max_speed = 27.777 * self.ext_env.resolution_scale  # ~100km/h
        d_env.min_speed = 27.777 * self.ext_env.resolution_scale

        self.ext_env.max_oob_time = 3
        self.learning_phase = "V"
        self.agent.learn(total_timesteps=400000)
        print(f"Finished learning phase V (100km/h) with max OOB time of: {self.ext_env.max_oob_time}")


        # ------------------- OCCLUSION ON ------------------- #
        self.int_env.occlusion = True

        # ---------------- PHASE VI ---------------- #
        self.ext_env.max_speed = 16.666 * self.ext_env.resolution_scale  # ~50km/h
        self.ext_env.min_speed = 16.666 * self.ext_env.resolution_scale

        self.ext_env.max_oob_time = 0.1
        self.learning_phase = "VI"
        self.agent.learn(total_timesteps=50000)
        print(f"Finished the learning phase VI (60km/h) with max OOB time of: {self.ext_env.max_oob_time}")

        # ---------------- PHASE VII ---------------- #
        self.ext_env.max_oob_time = 0.5
        self.learning_phase = "VII"
        self.agent.learn(total_timesteps=50000)
        print(f"Finished the learning phase VII (60km/h) with max OOB time of: {self.ext_env.max_oob_time}")

        # ---------------- PHASE VIII ---------------- #
        self.ext_env.max_oob_time = 5
        self.learning_phase = "VIII"
        self.agent.learn(total_timesteps=200000)
        print(f"Finished the learning phase VIII (60km/h) with max OOB time of: {self.ext_env.max_oob_time}")

        # ---------------- PHASE IX ---------------- #
        self.ext_env.max_speed = 22.222 * self.ext_env.resolution_scale  # ~80km/h
        self.ext_env.min_speed = 22.222 * self.ext_env.resolution_scale

        self.ext_env.max_oob_time = 4
        self.learning_phase = "IX"
        self.agent.learn(total_timesteps=300000)
        print(f"Finished the learning phase IX (80km/h) with max OOB time of: {self.ext_env.max_oob_time}")

        # ---------------- PHASE X ---------------- #
        self.ext_env.max_speed = 27.777 * self.ext_env.resolution_scale  # ~100km/h
        self.ext_env.min_speed = 27.777 * self.ext_env.resolution_scale

        self.ext_env.max_oob_time = 3
        self.learning_phase = "X"
        # (Optional: add callback=save_best_model_callback if you have a callback)
        self.agent.learn(total_timesteps=400000)
        print(f"Finished the learning phase X (100km/h) with max OOB time of: {self.ext_env.max_oob_time}")

        # ###############################################
        # # PHASE EXTRA RANDOM SPEED WITH ROLLOUT
        # ###############################################

        # # First we update the hyperparameters:

        # self.agent.save("agent_after_phase_X_model_X.zip") # CHANGE IF SIMULTANEOUS MODELS TRAINING

        # new_agent = PPO(
        #     "MultiInputPolicy",
        #     self,
        #     verbose=1,
        #     learning_rate=0.00022, # or 0.0001
        #     clip_range=0.1,
        #     ent_coef=0.005
        #     #n_steps = 1024 # if LR = 0.0001
        # )
        # old_agent = PPO.load("agent_after_phase_X_model_X.zip", env=self) # CHANGE IF SIMULTANEOUS MODELS TRAINING 
        # old_params = old_agent.get_parameters()
        # new_agent.set_parameters(old_params)
        # self.agent = new_agent

        # print("\n[INFO] Starting final Phase EXTRA: random speed training (60–100 km/h)\n")

        # # Now, set up a final training phase with random speeds between 60 km/h and 100 km/h
        # self.learning_phase = "EXTRA"
        # random_speed_callback = RandomSpeedCallback(env=self, min_kmh=60, max_kmh=100, verbose=1)

        # # Train for 400000 timesteps with the random-speed callback
        # self.agent.learn(total_timesteps=400000, callback=random_speed_callback)
        # print(f"Finished learning phase EXTRA (random 60–100km/h).")
        # ###############################################

        end = time.time()
        print(f"Training time: {end - start} seconds")
        print("All learning phases are COMPLETE. \\../")


    def print_simulation_status(self, a, r, d):
        # Formats and prints out the simulation stats after each timestep
        print(f"t: {self.ext_env.time:.1f}  v: {3.6 * self.ext_env.speed:.2f}  l.exc_o: {self.int_env.occ_lane_deviations}  l.exc_all: {self.int_env.total_lane_deviations}  "
            f"tlc: {self.ext_env.tlc:.2f}  off: {self.ext_env.offset:.3f}  r: {r:.2f}  a: [{a[0]:.2f}, {a[1]:.3f}, {a[2]:.2f}]  "
            f"head: {self.ext_env.heading:.2f}  pos: [{self.ext_env.pos[0]:.2f}, {self.ext_env.pos[1]:.2f}]  "
            f"oob: {self.ext_env.max_oob_time:.2f}  att: {'on' if self.int_env.has_attention else 'off'}")
    
    # Function to calculate and print averages
    def calculate_and_print_avgs(self, tlc_values, offset_values):
        # Calculating average TLC and offset if the corresponding list is not empty
        TLC_avg = sum(tlc_values) / len(tlc_values) if tlc_values else 0
        occ_offset_avg = sum(offset_values) / len(offset_values) if offset_values else 0
        abs_occ_offset_avg = sum([abs(o) for o in offset_values]) / len(offset_values) if offset_values else 0

        # Printing the calculated averages
        print(f"Average TLC: {TLC_avg:.3f} sec")
        print(f"Average Offset: {occ_offset_avg:.3f} meters")
        print(f"Average ABS Offset: {abs_occ_offset_avg:.3f} meters")

    # Function to calculate and print occlusion related data
    def calculate_and_print_occlusions(self):
        # If no occlusions completed, print a relevant message
        if self.ext_env.total_occlusions == 0:
            print("\nERROR: Infinite occlusion.")
        else:
            # Calculating average occlusion time, number of occlusions, and lane deviations
            avg_occlusion_time = self.ext_env.total_occlusion_duration / self.ext_env.total_occlusions
            num_occlusions = self.ext_env.total_occlusions
            num_lane_deviations = self.int_env.lane_deviations
            num_occ_lane_deviations = self.int_env.occ_lane_deviations
            num_total_lane_deviations = self.int_env.total_lane_deviations

            # Printing occlusion and lane deviation related data 
            print(f"\nAverage occlusion time: {avg_occlusion_time:.3f} sec")
            print(f"Number of occlusions: {num_occlusions}")
            print(f"\nNumber of lane deviations: {num_lane_deviations}")
            print(f"Number of lane deviations (occ): {num_occ_lane_deviations}")
            print(f"Number of total lane deviations: {num_total_lane_deviations}")

    # Simulates the agent for a given time or until done, logging the trace and returning the total reward, 
    # the done flag and the current time. DEBUG THIS FUNCTION TO SEE REWARDS ON LOCALS !!!
    def simulate(self, until = 120, get_trace = True, deterministic = True, inattention = []):
        self.reset() # Resetting the environment

        self.simulate_flag = True

        # Track variables before and after occlusions
        all_tlc_values = []
        before_occlusion_tlc_values, before_occlusion_offset_values = [], []
        after_occlusion_tlc_values, after_occlusion_offset_values = [], []
        
        occlusion_steering_values = []  # A list to store the steering values during an occlusion
        occlusion_steering_ranges = []  # A list to store the range of steering values for each occlusion

        reward = [] # Initialising list to hold rewards
        done = False # Initialising 'done' flag as False
        prev_attention = True  # Assumes the agent starts with full attention (DON'T CONFUSE with int_env's prev_attention_state)

        # Resets the rewards list and episode counter
        self.episode_rewards = []
        self.episode_counter = 0
        self.cumulative_reward = 0  # Initialize cumulative reward for the current episode
        self.total_episode_multiplier = 0 # Aid to get the total episode count

        ####################### THE LOOP UNTIL EPISODE IS DONE #######################

        # Keep simulating till 'done' flag becomes True
        while not done: # BREAKPOINT HERE TO STEP BY STEP @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            
            # Predict the next action based on the current observation and the deterministic parameter
            a, _ = self.agent.predict(self.int_env.obs, deterministic = deterministic) # FOR MANUALLY ADJUSTIN ACTIONS, SEE BELOW
            #a = [0.0, 0.26, 1] ### SET THE CUSTOM/MANUAL ACTION VALUES HERE (MANUALACC/MANUALSTEER/MANUALATT)
                                 ### ALSO REMEMBER TO SET d_env.max_steer & d_env.max_acceleration to 1 at testing
            
            # Call the step function with the predicted action, updating the environment, and get the reward and done flag
            _, r, d, _ = self.step(a) # STEPPAILU ALKAA TÄÄLTÄ, ARGUMENTTINA ACTION (1). LOPULTA PALAUTTAA S(ei käytetä atm), R, D JA INFO (11).
            
            all_tlc_values.append(self.ext_env.tlc)

            # Store steering values during occlusion
            if not self.int_env.has_attention:
                occlusion_steering_values.append(self.ext_env.steer)
            
            # Check if the agent is about to lose attention or just regained attention. Calculate and store the current values of TLC and offset
            if self.int_env.has_attention and not prev_attention:  # Just regained attention
                after_occlusion_tlc_values.append(self.ext_env.tlc)
                after_occlusion_offset_values.append(self.ext_env.offset)
                
                # When the occlusion ends, calculate the range and reset the occlusion_steering_values list
                max_steer = max(occlusion_steering_values) if occlusion_steering_values else 0
                min_steer = min(occlusion_steering_values) if occlusion_steering_values else 0
                occlusion_steering_ranges.append(max_steer - min_steer)
                occlusion_steering_values = [] # Reset for the next occlusion

            elif not self.int_env.has_attention and prev_attention:  # About to lose attention
                before_occlusion_tlc_values.append(self.ext_env.tlc)
                before_occlusion_offset_values.append(self.ext_env.offset)

            prev_attention = self.int_env.has_attention

            # Optionally log trace
            if get_trace: 
                self.log_trace() # If get_trace is True, log current state of the environment (POINTLESS?? NOT NEEDED FOR PLOT_TRACE!)

            self.print_simulation_status(a, r, d) # Print simulation status each line, starting from the 2nd timestep (after update_pos)

            # Add current reward to the reward list
            reward.append(r)

            # THE TERMINATION OF EPISODE HAPPENS HERE
            # If done flag is True or the environment's time exceeds a specified limit, set done to True
            if d or self.ext_env.time >= until:
                done = True

        ####################### WHEN THE EPISODE ENDS #######################

        # If the get_trace flag is set to True, this will turn off logging after the simulation is done
        if get_trace: 
            self.ext_env.log = False

        # Calculate and display the average occlusion time, number of occlusions, and number of lane deviations
        self.calculate_and_print_occlusions()

        # Calculate and print the average steering amplitude
        if occlusion_steering_ranges:
            avg_steering_amplitude = sum(occlusion_steering_ranges) / len(occlusion_steering_ranges)
        else:
            avg_steering_amplitude = 0
        print(f"\nAverage steering amplitude (occ): {avg_steering_amplitude:.3f}")

        all_tlc_values = sum(all_tlc_values) / len(all_tlc_values) if all_tlc_values else 0
        print(f"Average TLC (all): {all_tlc_values:.3f}")

        # Calculate and display averages of TLC and offset BEFORE occlusions
        print("\nBEFORE OCCLUSIONS:")
        self.calculate_and_print_avgs(before_occlusion_tlc_values, before_occlusion_offset_values)

        # Calculate and display averages of TLC and offset AFTER occlusions
        print("\nAFTER OCCLUSIONS:")
        self.calculate_and_print_avgs(after_occlusion_tlc_values, after_occlusion_offset_values)

        # Calculate total reward, simulation time, and average speed
        total_reward = round(sum(reward), 3)
        sim_time = round(self.ext_env.time, 1) # Because of dt = 0.1 this can have max. 1 decimal

        # Returns the total reward, done flag from the internal environment and simulation time
        return f"\nTOTAL REWARD: {total_reward:.3f}\nTOTAL TIME: {sim_time}\nDONE AREA: {self.int_env.within(self.ext_env.pos, self.int_env.done_area)}"
    
    # Takes an action, updates the state, reward, and done flag in the internal environment.
    # Checks if the episode has exceeded the maximum time or if the agent has been out-of-bounds 
    # for too long, and updates the done flag accordingly.
    def step(self, action):

        if self.int_env.occlusion == True:
            self.int_env.update_attention_state(self.int_env.has_attention)  # Call the update_attention_state method with passing self.int_env.has_attention
                                                                         # CONTINUES TO INT_ENV TO UPDATE ATTENTION STATE (2). >>> lisää int_env steppiin ???
        
        s, r, d = self.int_env.step(action) # Calls the step function of the driving_internal class with the given action.
                                            # and gets the updated state with get_belief (s), reward (r), and done flag (d).
                                            # TO INT_ENV'S STEP WITH ACTION (3). AFTER WHICH, RETURNS S, R, D (9).
        
        # Store TLC for the current timestep
        if not hasattr(self, 'episode_tlc_values'):
            self.episode_tlc_values = []
        self.episode_tlc_values.append(self.ext_env.tlc)

        # Track occlusion times if occlusion is active
        if self.int_env.occlusion:
            if not self.int_env.has_attention:
                self.current_occlusion_time += self.ext_env.dt
            else:
                if self.current_occlusion_time > 0:  # Occlusion just ended
                    self.occlusion_times.append(self.current_occlusion_time)
                    self.current_occlusion_time = 0  # Reset for next occlusion

        # Accumulate rewards for the current episode
        self.cumulative_reward += r

        # If the episode is done, print episode statistics
        if d:
            self.episode_rewards.append(self.cumulative_reward)
            avg_tlc = sum(self.episode_tlc_values) / len(self.episode_tlc_values) if self.episode_tlc_values else 0

            # If occlusions happened, calculate the average occlusion duration
            if len(self.occlusion_times) > 0:
                avg_occlusion_duration = sum(self.occlusion_times) / len(self.occlusion_times)
            else:
                avg_occlusion_duration = 0
            
            if self.simulate_flag == False:
                # Print info about the current episode in a single row
                print(f"EP: {self.episode_counter + 1} | "
                    f"R: {self.cumulative_reward:.2f} | "
                    f"TLC: {avg_tlc:.2f} s | "  # Average TLC of the episode
                    f"SPEED: {3.6 * self.ext_env.speed:.2f} km/h | "
                    f"OOB: {self.ext_env.max_oob_time:.2f} s | "
                    f"DONE: {self.int_env.within(self.ext_env.pos, self.int_env.done_area)} | "
                    f"DUR: {self.ext_env.time:.2f} s | "
                    f"OCCLUSION: {avg_occlusion_duration:.2f} s | "  # Average occlusion duration
                    f"PHASE: {self.learning_phase}")
                
            # Tallenna tiedot CSV-tiedostoon
            with open(self.csv_file, mode='a', newline='') as file:
                writer = csv.writer(file)
                writer.writerow([self.episode_counter + 1,  # Episodin numero
                                self.cumulative_reward,   # Kumulatiivinen reward
                                avg_tlc,                  # Keskimääräinen TLC
                                self.ext_env.speed,       # Nopeus
                                self.ext_env.max_oob_time, # Max OOB time
                                self.int_env.within(self.ext_env.pos, self.int_env.done_area), # Done-tila
                                self.ext_env.time,        # Kesto
                                avg_occlusion_duration,   # Average occlusion duration per episode
                                self.int_env.occlusion,   # Occlusion-tila
                                self.learning_phase])     # Oppimisvaihe

            # Reset for the next episode
            self.episode_counter += 1
            self.occlusion_times = []  # Reset occlusion times for the next episode
            self.episode_tlc_values = []  # Reset TLC values for the next episode
            self.cumulative_reward = 0    # Reset cumulative reward for the next episode
            self.current_occlusion_time = 0  # Reset current occlusion time

        return s, r, d, {} # RETURNS THE UPDATED STATE, REWARD, DONE FLAG AND EMPTY DICTIONARY FOR INFO BACK TO SIMULATE (10).
                           # s = state (includes tlc, inattention, speed, heading, steer FROM get_belief-function)

    # NOTE ABOUT STEP FUNCTIONS: 
    # The step function in the driving_internal class is responsible for updating the internal state of the driving model, applying the action to the external environment, and computing the reward.
    # The step function in the driving_environment class updates the state of the external environment, such as the position, speed, and heading of the car, based on the given action.
    # Each class has its own step function because they each handle different aspects of the simulation. The driving_agent class is the highest level of abstraction and interacts with the reinforcement learning algorithm. 
    # Its step function communicates with the driving_internal class, which in turn communicates with the driving_environment class. This separation of concerns allows for better organization and modularity in the code.

# This code at the end of the file is used for debugging and testing the agent. The conditional ensures that the code inside is 
# only executed when the script is run directly (i.e., python driving_agent.py), and not when it's imported elsewhere.
if __name__ == "__main__":

    d_env = driving_environment.driving_environment(area_bounds = 1650 * 1)
    #d_env.adas.append('lane_watch') # IF YOU WANNA ENABLE THE LANE WATCH
    d_env.start_heading = -1.08 # equals to 5.2 RADIANS
    d_env.start_pos = [328.5 * d_env.resolution_scale, 804.5 * d_env.resolution_scale] # ~1km until done area
    d_env.steer_noise = 0.003 # Steering wheel related constant noise + action noise when steering. VARY BETWEEN 0.001 to 0.01. #CHANGE THIS

    map_generator.make_circular_road(d_env, 1600 * d_env.resolution_scale, 1600 * d_env.resolution_scale, d_env.track_radius, d_env.track_width)
    
    d_int = driving_internal.driving_internal(d_env)
    d_int.reset()

    # Add waypoint [X-startpoint, Y-startpoint, X-endpoint, Y-endpoint]
    d_int.add_waypoint([380 * d_env.resolution_scale, 723 * d_env.resolution_scale, 385 * d_env.resolution_scale, 725 * d_env.resolution_scale])
    d_int.add_waypoint([530 * d_env.resolution_scale, 545 * d_env.resolution_scale, 535 * d_env.resolution_scale, 547 * d_env.resolution_scale])
    d_int.add_waypoint([795 * d_env.resolution_scale, 331 * d_env.resolution_scale, 800 * d_env.resolution_scale, 333 * d_env.resolution_scale])
    d_int.done_area = [1091 * d_env.resolution_scale, 184 * d_env.resolution_scale, 1098 * d_env.resolution_scale, 191 * d_env.resolution_scale]
    d_env.max_speed = 17 * d_env.resolution_scale # Max speed of the agent
    d_env.min_speed = 16 * d_env.resolution_scale # Min speed of the agent

    # Save directory setup
    save_dir = "/Users/tonialha/Documents/CODEFOLDER/MODEL_DATA/SAVED_MODELS/" # Model save path here #CHANGE THIS
    os.makedirs(save_dir, exist_ok=True)  # Creates the directory if it doesn't exist

    # Define paths for the model and CSV file
    save_path = os.path.join(save_dir, "model_X.pkl")  # NEW FILE NAME HERE #CHANGE THIS
    csv_file_name = os.path.join(save_dir, "training_log_model_X.csv")  # NEW CSV FILE NAME HERE # CHANGE

    # Initialize the driving agent with the CSV file name
    d_agent = driving_agent(d_int, csv_file=csv_file_name)
    d_agent.train_curriculum()  # Train and save the best model using the custom callback

    d_agent.BEST = False # USE THE BEST OF THE BEST POLICY #@BEST

    #Path to the saved best policy model, adjusted as per where SaveBestModelCallback saves it @BEST
    best_model_path = "./checkpoints/best_model_X.zip"  ### CHANGE 2/2 e.g. "best_model1, best_model2..etc" IF MULTIPLE SIMULTANEOUS RUNS! LOADS HERE. 
    if d_agent.BEST and os.path.exists(best_model_path):
        d_agent.agent = PPO.load(best_model_path, env=d_agent) # Load the best policy for simulation
    else:
        print("Best policy could not be found. Using the latest learned policy.")

    #SIMULATION
    d_env.max_oob_time = 5
    print(d_agent.simulate(get_trace=True, deterministic=True))
    d_env.plot_trace(full_area=True, waypoints=d_int.waypoints, done_area=d_int.done_area)
    animate_trace.animate_trace(d_env, d_agent.trace)

    try:
        # Save the best performing agent to a file
        d_agent.agent.save(save_path)
        print(f"Model saved to {save_path}")
    except Exception as e:
        print(f"Failed to save the model: {e}")


# FINE TUNE:
# max_steer (0.08 - 0.62 RAD)
# d_env.steer_noise = (0.005)
# d_env.steer_action_noise (0.001 - 0.01)
# inattention_reward (0.05 - 0.5)
# get_reward > offset (0.1 - 0.25) && tlc (0.5 - 1)
# get_reward > Negative reward when ending the occlusion to prevent switching ON/OFF rapidly (neg. 1 - 5)
# train_curriculum > self.agent.learn(total_timesteps = ~500000)
# learning_rate (0.0001 - 0.0005)

# ADDITIONALLY TUNE:
# d_env.adas.append('lane_watch’) # kommentoi POIS jos OFF
# heading, positio sekä erinäiset nopeusparametrit (min, max, limit ja start speed) tehtävän mukaan

# SCRAPPED FUNCTIONS:
# def predict_value(self):
#     values = self.agent.policy.forward(self.agent.policy.obs_to_tensor(self.driver.get_belief())[0])
#     return values.item()

# def train_agent(self, max_iters = 20, debug = False):
#     done = False
#     i = 0
#     while not done:
#         self.agent.learn(total_timesteps = 50000)
#         i+=1
#         if debug:
#             print(i, self.simulate())
#         if i == max_iters:
#             done = True

# Generate either circular_road or straight_road
# if random.randint(1,3)==1:
#     d_env.start_heading = 5.2
#     d_env.start_pos = [327,808]
#     print(map_generator.make_circular_road(d_env, 1600, 1600, 1500, 5))
#     d_int.done_area = [1000, 210, 1030, 240]
# elif random.randint(1,3)==2:
#     d_env.start_heading = 0
#     d_env.start_pos = [505,505]
#     print(map_generator.make_straight_road(d_env, 500, 500, 1500, 500, 10))
#     d_int.done_area = [1440, 490, 1460, 510]
# else:
#     d_env.start_heading = 1.57
#     d_env.start_pos = [750,105]
#     print(map_generator.make_rectangle_road(d_env, 745, 100, 755, 1500))
#     d_int.done_area = [740, 1440, 760, 1460]

#d_int.add_waypoint([850, 280, 875, 310])
#d_int.add_waypoint([925, 250, 950, 275])



        # ADD THIS BELOW TIME IN TRAIN CURRICULUM:

        # # Speed ranges corresponding to 60, 80, and 100 km/h
        # speed_ranges = [(16, 17), (22, 23), (27, 28)]

        # # Define the starting and ending OOB times
        # oob_start = 0.1  # Starting OOB time
        # oob_end = 3  # Final OOB time

        # # There are 12 increments per set (4 phases * 3 increments)
        # total_increments = 12 # number of increments for each speed * 3 speeds = 36

        # # Calculate OOB time increment per step
        # oob_increment_per_step = (oob_end - oob_start) / (total_increments - 1)

        # # Function to calculate the OOB time for a specific increment and phase
        # def get_oob_time(phase, increment):
        #     # Calculate the step in the progression from 0.1 to 3 for each phase and increment
        #     return oob_start + ((phase - 1) * 3 + increment) * oob_increment_per_step

        # for phase in range(1, 5):  # For the first four learning phases
        #     base_oob_time = get_oob_time(phase, 0)  # Get base OOB time for the phase
        #     for speed_min, speed_max in speed_ranges:
        #         for increment in range(3):  # Gradually increase speed within each sub-phase
        #             # Progressively increase speed range
        #             d_env.max_speed = speed_max + increment * 1
        #             d_env.min_speed = speed_min + increment * 1

        #             # Calculate OOB time for the current increment within the speed range
        #             self.ext_env.max_oob_time = base_oob_time + increment * oob_increment_per_step

        #             # Train with random speeds from the range and progressive difficulty
        #             self.learning_phase = f"Phase_{phase}_Speed_{speed_max * 3.6:.0f}kmh_Increment_{increment}"
        #             self.agent.learn(total_timesteps=50000)  # 100,000 timesteps per sub-phase
        #             print(f"Finished {self.learning_phase} with max OOB time of: {self.ext_env.max_oob_time}")

        # # Train with occlusion (Phases 5-8)
        # self.int_env.occlusion = False

        # for phase in range(5, 9):  # For the occlusion phases
        #     base_oob_time = get_oob_time(phase - 4, 0)  # Get base OOB time for occlusion phase (equivalent to phase 1-4)
        #     for speed_min, speed_max in speed_ranges:
        #         for increment in range(3):  # Gradually increase speed within each sub-phase
        #             # Progressively increase speed range
        #             d_env.max_speed = speed_max + increment * 1
        #             d_env.min_speed = speed_min + increment * 1

        #             # Calculate OOB time for the current increment within the speed range
        #             self.ext_env.max_oob_time = base_oob_time + increment * oob_increment_per_step

        #             # Train with random speeds from the range and progressive difficulty
        #             self.learning_phase = f"Occlusion_Phase_{phase}_Speed_{speed_max * 3.6:.0f}kmh_Increment_{increment}"
        #             self.agent.learn(total_timesteps=50000)  # 100,000 timesteps per sub-phase
        #             print(f"Finished {self.learning_phase} with max OOB time of: {self.ext_env.max_oob_time}")