import numpy as np
import math
import copy
import map_generator

import matplotlib
import matplotlib.pyplot as plt

# PART 1/3: THE ENVIRONMENT
# Implementation of an external driving environment.
class driving_environment():
    def __init__(self, area_bounds = 1000 * 1):

        # Adjust scaling for finer resolution
        self.resolution_scale = 1  # 1 unit = 10 cm
        
        self.wheelbase = 2.7 * self.resolution_scale # Distance between front and backwheels (derieved from the LUA: (front_axle = 1.11176) - (rear_axle = -1.58824) = 2.7 meters)
        self.front_axle = 1.11176 * self.resolution_scale # + from pos (used instead of wheelbase)
        self.rear_axle = 1.58824 * self.resolution_scale # - from pos (used instead of wheelbase)

        self.dt = 0.1 # Delta time, a change in time in seconds (model's numerical values are recalculated every 0.1s or 100ms), basically 
                      # the duration of one timestep

        self.trace = [] # Iniates trace array

        self.max_steer = 0.26 # in RAD, aproximately 15 degrees (positive & negative, equals 30 degrees) per second which is the max of the avg steering amplitude's range. ALT: 0.174533 RAD.

        self.steer_noise = 0.0 # Adjust this in the test block at the end of driving_agent

        self.start_heading = 0 # Straight to right e.g. 0 RAD, down 1.57 RAD (90 deg), left 3.14 RAD (180 deg), up 4.71 RAD (270 deg)
                               # BUT the displayed range here is between -π and π (from negative 3.141 to positive 3.141) !!!
        self.previous_heading = 0  # Initialize previous heading

        # How many m/s can be accelerated in dt time
        self.max_acceleration = 0.5 * self.resolution_scale # 0.5m/s per dt(0.1s) so one second (10 X dt) equals 5m/s so acceleration to 100km/h (= 27.77m/s) is 27.77/5 = 5.55 seconds

        self.log = True # To log the state of the agent 

        self.area_bounds = area_bounds # How many metres from the top right corner does the area span
        self.track_width = 3.75 * self.resolution_scale # The track width
        self.track_radius = 1500 * self.resolution_scale # The curvature of the track 

        self.start_pos = [0,0] # pos[0] = x, pos[1] = y

        # How close to the end of the area can the car drive, "invicible wall"
        self.pos_bounds = 20 * self.resolution_scale

        # Empty area.
        # 1 = road
        # 2 = out of road
        # Initializes the driving environment grid with a specific value.
        self.area = np.zeros(shape=(self.area_bounds,self.area_bounds)) + 2 # Every element in the 2D numpy array will have a value of 2.

        self.max_area_code = 2 # Area outside the road (1 = at the road; found in map_generator) AREA CODE 2 (also color)

        self.offset = 0
        self.tlc = 0
        
        self.max_speed = 60 / 3.6 # Max speed of the agent in ms (fixed at 60kmh)
        self.min_speed = 60 / 3.6 # Min speed of the agent in ms (fixed at 60kmh)

        self.max_oob_time = 0.0 # Max time allowed outside of the road
        self.current_oob_time = 0 # Track how long the agent stays out of bounds (OOB)

        self.prev_steer = 0.0 # Store the previous steering angle, updated in the step function
        
        # Advanced driver-assistance system
        self.adas = []

        self.use_random_speed = False  # default: do not randomize

        self.reset() # Resets the driving_environment
        
    def reset(self, heading=False):
        import random

        if self.use_random_speed:
            # 60–100 km/h  ==>  16.67–27.78 m/s
            speed_ms = random.uniform(60.0/3.6, 100.0/3.6)

            # Force the min_speed and max_speed to be exactly that speed:
            self.min_speed = speed_ms
            self.max_speed = speed_ms

            # Start the car at that exact speed
            self.speed = speed_ms
        else:
            # e.g. your old fixed speeds or default
            self.speed = 0.0

    # -- Now proceed with your normal reset logic --
        self.prev_steer = 0.0

        self.pos = copy.copy(self.start_pos)

        self.trace = [] # Same as in the init, but resets the array

        self.limited = False # Should restrict the position into certain bounds, so the agent doesn't deviate too far from the road  

        self.heading = self.start_heading # COUNTER-CLOCKWISE ) = negative, ( = positive, π = UP, 0 = DOWN
                                          # CLOCKWISE ) = positve, ( = negative, π = DOWN **EXPLAINED MORE IN DETAIL IN THE END OF CODE
        self.previous_heading = self.start_heading
        self.steer = 0 # Negative = turns to left, Positive = turns to right

        self.time = 0
        self.current_oob_time = 0  # Reset OOB time on environment reset

        # Tracks the number of time steps the agent has had attention / inattention
        self.attention_counter = 0
        self.inattention_counter = 0

        self.total_occlusion_duration = 0
        self.total_occlusions = 0
        self.timestep_count = 0

        self.front_wheel = [self.wheelbase/2, 0] # Distance of front wheels from pos (+XXXm up from the center) ALT: [self.front_axle, 0]
        self.back_wheel = [-self.wheelbase/2, 0] # Distance of back wheels from pos (-XXXm from the center) ALT: [-self.rear_axle, 0]

        self.offset = self.calculate_offset(self.pos[0], self.pos[1])
        self.tlc = self.calculate_dynamic_tlc()

    # Updates the actual state of the car in the environment. This includes calculating the new positions of the front and back wheels, the new position 
    # of the car (which is the midpoint between the front and back wheels IF using wheelbase), and updating the car's heading based on these positions.
    def update_pos(self):

        # Determine the steering angle (OG BLOCK 1/2)
        steer = np.random.logistic(self.steer, self.steer_noise) # Simulates driving wheels random fluctuation noise
        if 'lane_watch' not in self.adas:
            steer += abs(self.steer)*np.random.logistic(0, self.steer_noise) # Added noise on top of steer that scales by how much the driving wheel is rotated.

        # Ensures that the steering angle doesn't exceed the car's physical steering limits (OG BLOCK 1/2)
        steer = max(steer, -self.max_steer) # value is not less than the negative value of the maximum steering angle. If steer is less than -self.max_steer, it will be set to -self.max_steer
        steer = min(self.max_steer, steer) # value is not greater than the maximum steering angle. If steer is greater than self.max_steer, it will be set to self.max_steer
        
        # Calculate the new position of the front wheel (simple bicycle model), [0] = x [1] = y
        self.front_wheel[0] = self.pos[0] + self.wheelbase/2 * math.cos(self.heading) # horizontal component of the front wheel's direction. or self.front_axle
        self.front_wheel[1] = self.pos[1] + self.wheelbase/2 * math.sin(self.heading) # vertical component of the front wheel's direction. or self.front_axle
        self.front_wheel[0] += self.speed * self.dt * math.cos(self.heading + steer) # Determines how much of vehicle's motion is in the left-right direction relative to the vehicle's current heading and steering angle.
        self.front_wheel[1] += self.speed * self.dt * math.sin(self.heading + steer) # Determines how much of vehicle's motion is in the forward-backward direction relative to the vehicle's current heading and steering angle.

        # Calculate the new position of the back wheel (simple bicycle model) [0] = x [1] = y
        self.back_wheel[0] = self.pos[0] - self.wheelbase/2 * math.cos(self.heading) # or self.rear_axle
        self.back_wheel[1] = self.pos[1] - self.wheelbase/2 * math.sin(self.heading) # or self.rear_axle
        self.back_wheel[0] += self.speed * self.dt * math.cos(self.heading)
        self.back_wheel[1] += self.speed * self.dt * math.sin(self.heading)

        # Update the car's actual position and check if it is limited (= car's position is within the defined environment boundaries)
        self.pos[0] = (self.front_wheel[0] + self.back_wheel[0])/2
        self.pos[1] = (self.front_wheel[1] + self.back_wheel[1])/2
        self.limited = False
        if self.pos[0] < self.pos_bounds:
            self.limited = True
            self.pos[0] = self.pos_bounds
        if self.pos[1] < self.pos_bounds:
            self.limited = True
            self.pos[1] = self.pos_bounds
        if self.pos[0] > self.area_bounds - self.pos_bounds:
            self.limited = True
            self.pos[0] = self.area_bounds - self.pos_bounds
        if self.pos[1] > self.area_bounds - self.pos_bounds:
            self.limited = True
            self.pos[1] = self.area_bounds - self.pos_bounds
        
        # Update the car's heading
        self.heading = math.atan2(self.front_wheel[1] - self.back_wheel[1],
                                self.front_wheel[0] - self.back_wheel[0])

    # Simulates car's movement by updating the position and heading of the car based on the given speed, steering angle, and other parameters.
    # Useful for predicting the future position of the car based on given parameters without affecting the actual state of the car.
    def simulate_pos_update(self, speed, pos, steer, steer_noise, max_steer, heading, front_wheel, back_wheel, wheelbase, dt):

        # Add noise to the steering angle based on steer_noise and steer_action_noise (OG BLOCK 1/2)
        steer = np.random.logistic(steer, steer_noise) # Generates a random number from logistic distribution with a mean of steer and a scale of steer_noise.
        steer += abs(steer)*np.random.logistic(0, steer_noise) # Scales the random value from the logistic distribution by the absolute value of the current steer.
                                                                      # Then adds the scaled value to the existing steer value

        # Ensure the steer value stays within the range of -max_steer to max_steer (2/2 OG)
        steer = max(steer, -max_steer) # Returns the larger of its two arguments, so steer does not go below -max_steer.
        steer = min(max_steer, steer) # Returns the smaller of its two arguments, so steer does not exceed max_steer.

        if steer != 0:
            R = wheelbase / np.tan(steer) # Calculate the radius of curvature based on the tangent of the steering angle.
            delta_theta = speed * dt / R # Calculates delta_theta, which represents the change in orientation or heading of the vehicle.

            # Compute the new position based on a curved trajectory
            pos[0] += R * (np.sin(heading + delta_theta) - np.sin(heading))
            pos[1] -= R * (np.cos(heading + delta_theta) - np.cos(heading))
            heading += delta_theta
        
        else:
            # Move straight
            pos[0] += speed * dt * np.cos(heading)
            pos[1] += speed * dt * np.sin(heading)

        # Calculate the current position of the front and back wheel
        front_wheel[0] = pos[0] + self.wheelbase/2 * np.cos(heading)
        front_wheel[1] = pos[1] + self.wheelbase/2 * np.sin(heading)
        back_wheel[0] = pos[0] - self.wheelbase/2 * np.cos(heading)
        back_wheel[1] = pos[1] - self.wheelbase/2 * np.sin(heading)

        # Return the updated position, front_wheel, back_wheel, and heading
        return pos, front_wheel, back_wheel, heading

###########################################################

    # Lane watch works by returning a steer that, within limits, attempts to maximise TLC. Basically: ENABLE TO DRIVE SMOOTHLY! 
    def lane_watch(self):        
        # Can be set to smaller values for a less aggressive lane watch.
        # steers = np.linspace(-self.max_steer, self.max_steer, 10)
        # steers = np.append(steers, 0)

        steers = [-0.5, 0, 0.5]
        # Select the TLC that best, out of four steers (current steer,
        # 0, min or max) maximises TLC.
        tlc = {}
        for s in steers:
            tlc[s] = self.calculate_dynamic_tlc(heading = s)

        #print(tlc)
        return max(tlc, key = tlc.get)

###########################################################
    
    # Gives the area of the specific coordinates provided (pos) OR gives the area of the agent's current location self.pos (current_area()).
    def current_area(self, pos=None):
        # If no position is given, use the current position of the agent
        if pos is None: 
            pos = self.pos

        # Constants for the center of the circle, radius, and width of the road
        x0, y0, r, w = 1600 * self.resolution_scale, 1600 * self.resolution_scale, self.track_radius, self.track_width 

        # Calculate the distance from the center of the circle
        d = np.sqrt((pos[0] - x0)**2 + (pos[1] - y0)**2)

        # Calculate radial distance from the center of the road
        dr = d - r

        # Check if the vehicle is outside the road boundaries
        # Instead of checking against offset, we use the normalized distance (dr) and the half-width of the road (w/2)
        if abs(dr) >= w/2:
            return 2  # out of road
        # If the positional offset is within the bounds of the area, return 1 (at the road)
        else:
            return 1

        # # Check if the position values are within the area array bounds before attempting to access the area array.
        # if 0 <= round(pos[0]) < len(self.area) and 0 <= round(pos[1]) < len(self.area[0]):
        #     alue = self.area[round(pos[0])][round(pos[1])]
        #     return alue  # If within area bounds, return the area code from the area matrix at the (int) position of the agent
        
    # Used to calculate the lateral position (left/right offset) of a vehicle relative to the center of a circular lane. 
    # This function assumes that the road is a circular track with a known center (x0, y0), radius (r), and width (w).
    def calculate_offset(self, x, y):
        x0, y0, r, w = 1600 * self.resolution_scale, 1600 * self.resolution_scale, self.track_radius, self.track_width  # Center of the circle, radius, and width of the road

        # Calculate distance from the center of the circle
        d = np.sqrt((x - x0)**2 + (y - y0)**2)

        # Calculate radial distance from the center of the road
        dr = d - r

        # Calculate normalized offset and multiply by -1 to swap the signs (so left border = -1 and right border = 1)
        offset = -dr / (w / 2) # Basically the radial distance divided by half of the width of the road, so at the 1.875 off = 1 etc.

        return offset

    # Computes the time it would take for the car to cross a lane based on its current state (position, heading, etc.) and a potential additional change in its heading. 
    # The purpose of this function is to make more realistic estimates of lane crossing times that take into account changes in steering angle.
    def calculate_dynamic_tlc(self, additional_heading=0, dt_multiplier=10, pos=None):
        
        # Set pos variable for simulation with current actual pos, and in a similar manner set the new 
        # f_w and b_w variables with the front and back wheel positions, as well as heading and steer
        pos = np.array(self.pos)
        f_w = np.array(self.front_wheel)
        b_w = np.array(self.back_wheel)
        heading = self.heading + additional_heading # Update the current heading with additional_heading                                      # (if provided; for tlc_r and tlc_l)
        steer = self.steer
        dt = self.dt / dt_multiplier # For more precise tlc-values

        # Begin iteration, check if pos OOB and return iterations * dt:
        max_simulation_steps = int(70 / dt) # Assuming dt is in seconds and you want a max tlc of 70 seconds

        for i in range(max_simulation_steps): # For maximum of 700 timesteps

            if self.current_area(pos) == 2: # Check if the position is out of bounds
                return i * dt # Returns the number of timesteps in the simulation until oob * dt = TLC in seconds
            if self.speed <= 1:
                return i * dt 

            # MAIN PART OF THE FUNCTION STARTS HERE: THE SIMULATION!
            # Use the simulate_pos_update function to updates vehicle's position, front wheel position, 
            # back wheel position, and heading based on the current speed, position, steering angle, etc.
            pos, f_w, b_w, heading = self.simulate_pos_update(
                self.speed,
                pos,
                steer,
                0, # Noises are 0 in the internal belief update 
                self.max_steer,
                heading,
                f_w,
                b_w,
                self.wheelbase,
                dt
            )
            
        # If iteration ends without crossing the boundary, return the max tlc
        return max_simulation_steps * dt

    # Visualizes the car's movement in the environment, allowing you to see the trajectory, waypoints, and target area.
    def plot_trace(self, full_area=False, waypoints=[], done_area=None):
        plt.close()

        # Determine plotting boundaries
        if not full_area and self.trace:
            x_min = None
            x_max = None
            y_min = None
            y_max = None
            for i in range(len(self.trace)):
                pos_x = self.trace[i][1]["pos"][0]
                pos_y = self.trace[i][1]["pos"][1]
                if x_min is None or x_min > pos_x:
                    x_min = round(pos_x) - 1 * self.resolution_scale
                if x_max is None or x_max < pos_x:
                    x_max = round(pos_x) + 1 * self.resolution_scale
                if y_min is None or y_min > pos_y:
                    y_min = round(pos_y) - 1 *self.resolution_scale
                if y_max is None or y_max < pos_y:
                    y_max = round(pos_y) + 1 * self.resolution_scale
        else:
            x_min = 0
            x_max = self.area_bounds
            y_min = 0
            y_max = self.area_bounds

        # Create plot_area array
        plot_area = np.zeros(shape=(x_max - x_min, y_max - y_min))

        # Draw the area
        for i in range(plot_area.shape[0]):
            for j in range(plot_area.shape[1]):
                plot_area[i, j] = self.area[x_min + i, y_min + j]

        # Add waypoints
        for wp in waypoints:
            x_start, y_start, x_end, y_end = wp
            x_start, y_start = max(0, x_start - x_min), max(0, y_start - y_min)
            x_end, y_end = min(plot_area.shape[0], x_end - x_min), min(plot_area.shape[1], y_end - y_min)
            plot_area[x_start:x_end, y_start:y_end] = 12  # AREA CODE 12 (waypoints)

        # Add done area
        if done_area:
            x_start, y_start, x_end, y_end = done_area
            x_start, y_start = max(0, x_start - x_min), max(0, y_start - y_min)
            x_end, y_end = min(plot_area.shape[0], x_end - x_min), min(plot_area.shape[1], y_end - y_min)
            plot_area[x_start:x_end, y_start:y_end] = 12  # AREA CODE 12 (done area)

        # Add trace points
        for i in range(len(self.trace)):
            pos_x = round(self.trace[i][1]["pos"][0] - x_min)
            pos_y = round(self.trace[i][1]["pos"][1] - y_min)
            if 0 <= pos_x < plot_area.shape[0] and 0 <= pos_y < plot_area.shape[1]:
                plot_area[pos_x, pos_y] = 6  # AREA CODE 6 (trace points)

        # Add current position if no trace
        if not self.trace:
            pos_x = round(self.pos[0] - x_min)
            pos_y = round(self.pos[1] - y_min)
            if 0 <= pos_x < plot_area.shape[0] and 0 <= pos_y < plot_area.shape[1]:
                plot_area[pos_x, pos_y] = 5  # AREA CODE 5 (current position)

        # Plot the area
        plt.imshow(plot_area.transpose(), norm=matplotlib.colors.Normalize(vmin=0, vmax=12), cmap='seismic')
        plt.show()  # Breakpoint here to get the visualization of the trace while debugging
        return

    
    # Encapsulates and returns the current state of the car in the environment into a single dictionary
    def get_state(self):
        return {"pos":self.pos, # The current position of the car in the environment (a list with two elements, [x, y]).
                "speed":self.speed, # The current speed of the car.
                "heading":self.heading, # The current heading of the car (the angle at which the car is pointing).
                "steer":self.steer} # The current steering angle of the car.

    # Action is a vector [speed,steer].
    def step(self, action): # WITH THE ACTION ORIGINALLY GIVEN FROM AGENT, UPDATED IN THE INT_INV, UPDATE THE STATE VARIABLES (6).

        # Update the speed of the car based on the first element of the action vector, action[0]. 
        # The speed is then clipped between self.min_speed and self.max_speed to ensure it remains within the allowed limits.
        self.speed += action[0] # Speed is updated based on acceleration
        self.speed = max(self.speed, self.min_speed)
        self.speed = min(self.max_speed, self.speed)

        # Update the steering based on the second element of the action vector, action[1]
        self.steer = action[1]

        # If the 'lane_watch' ADAS (Advanced Driver-Assistance System) is enabled, 
        # adjust the steering value accordingly using the self.lane_watch() function.
        if 'lane_watch' in self.adas:
            self.steer = self.lane_watch()        

        # Update the position of the car based on the current speed and steering values.
        self.update_pos()

        # Get the current state of the car/environment.
        state = self.get_state()

        self.offset = self.calculate_offset(self.pos[0], self.pos[1])
        self.tlc = self.calculate_dynamic_tlc()

        # If logging is enabled, calculate tlc and offset and append the current state to the self.trace array.
        if self.log:
            state['tlc'] = self.tlc
            state['offset'] = self.offset

            self.trace.append([self.time, copy.deepcopy(state)])
        
        # Store the current steering angle as the previous steering angle
        self.prev_steer = self.steer

        # Increment the simulation time by the time step, self.dt
        self.time += self.dt

        # Check whether the car is out of bounds
        if self.current_area() == 1:
            self.current_oob_time = 0
        else:
            self.current_oob_time += self.dt

        # RETURN THE UPDATED STATE TO THE INTERNAL ENVIRONMENT (7).
        return state
    
# ** MORE IN DEPTH HEADING EXPLANATION:

# Radian Measure and Circle Orientation:
# In mathematics, angles measured in radians are often based on the unit circle, where 1 radian is the angle subtended at the center of a circle by an arc whose length is equal to the radius of the circle. By convention, positive angles are measured counter-clockwise from a reference line (usually the positive x-axis), and negative angles are measured clockwise.

# Counter-Clockwise Movement:
# When the car is moving counter-clockwise, a positive heading (like a curve "(") indicates a left turn from the car's perspective, aligning with the standard mathematical convention of positive angles. A negative heading in a counter-clockwise direction (like a curve ")") indicates a right turn, which is opposite to the direction of movement and thus negative.

# Clockwise Movement:
# Conversely, when the car is moving clockwise, a positive heading (like a curve ")") indicates a right turn, which aligns with the clockwise direction and thus is considered positive. A negative heading in a clockwise direction (like a curve "(") indicates a left turn, opposite to the clockwise direction, hence negative.

# π = Up or Down:
# A heading of π radians (or -π) represents a half-circle turn. In a counter-clockwise movement, π represents a turn upwards (if starting from the right), and in a clockwise movement, it represents a turn downwards (if starting from the left). This is because π radians is 180 degrees, so it inverts the direction of movement, making the car face the opposite direction.


#-------------------------------------------------

# d_env = driving_environment(area_bounds = 1000)
# d_env.speed = 17

# d_env.log = True
# d_env.pos = [100, 500]
# for i in range(100):
#     d_env.step([0,0])
# for i in range(5):
#     d_env.step([0,0.1])
# for i in range(100):
#     d_env.step([0,0])

# for i in range(10):
#     d_env.step([0,0])
# for i in range(10):
#     d_env.step([0,-0.1])
# for i in range(10):
#     d_env.step([1,0])

# d_env.start_pos = [200,210]
# map_generator.make_circular_road(d_env, 500, 100, 700, 50)
# map_generator.make_straight_road(d_env, 100, 500, 700, 500, 50)
# a = d_env.plot_trace(full_area = True)

#-------------------------------------------------

# d_env = driving_environment(area_bounds = 1650)
# d_env.adas.append('lane_watch')
# d_env.start_heading = 5.3
# d_env.start_pos = [327,808] # farther away
# d_env.steer_noise = 0.005
# d_env.steer_action_noise = 0.005

# map_generator.make_circular_road(d_env, 1600, 1600, 1500, 5)

# d_env.reset()
# d_env.speed = 20
# d_env.log = True
# d_env.trace = []
# for i in range(500):
#     d_env.step([0,0.1])
# d_env.plot_trace()

#-------------------------------------------------

    # Given current position and heading (but not considering current
    # steer), how long until lane crossing?
    # def time_to_lane_crossing(self):
    #     if self.current_area() == 2:
    #         return 0
    #     done = False
    #     time = 0
    #     front_wheel = copy.deepcopy(self.front_wheel)
    #     back_wheel = copy.deepcopy(self.back_wheel)
    #     pos = copy.deepcopy(self.pos)
    #     while not done:
    #         time+=self.dt
    #         front_wheel[0] = pos[0] + self.wheelbase/2 * math.cos(self.heading)
    #         front_wheel[1] = pos[1] + self.wheelbase/2 * math.sin(self.heading)
    #         front_wheel[0] += self.speed * self.dt * math.cos(self.heading + self.steer)
    #         front_wheel[1] += self.speed * self.dt * math.sin(self.heading + self.steer)
    #         back_wheel[0] = pos[0] - self.wheelbase/2 * math.cos(self.heading)
    #         back_wheel[1] = pos[1] - self.wheelbase/2 * math.sin(self.heading)
    #         back_wheel[0] += self.speed * self.dt * math.cos(self.heading)
    #         back_wheel[1] += self.speed * self.dt * math.sin(self.heading)

    #         pos[0] = (front_wheel[0] + back_wheel[0])/2 # eli pos[0] sekä
    #         pos[1] = (front_wheel[1] + back_wheel[1])/2 # pos[1] ovat auton etu- ja takarenkaiden välissä

    #         time += self.dt
    #         if time > 100: return 100
    #         if self.current_area(pos = pos) == 2:
    #             return time

#-------------------------------------------------
    
    # # Trigonometric method for calculating tlc, during normal driving on a curved path. DRAFT.
    # def calculate_trigonometric_tlc(self, additional_heading=0, pos=None):
    #     # Speed check
    #     if self.speed == 0:
    #         return float('inf')  # Infinite if speed is zero

    #     # Current vehicle position
    #     # Optionally update the position from the args
    #     if pos is not None:
    #         x, y = pos
    #     else: # default
    #         x, y = self.pos
        
    #     # Calculate the yaw rate as the change in heading over time (yaw rate per second)
    #     # Unwrap the current and previous heading to avoid jumps from -π to π
    #     current_heading_unwrapped = np.unwrap([self.previous_heading, self.heading])[1]
    #     yaw_rate = (current_heading_unwrapped - self.previous_heading) / self.dt
    #     self.previous_heading = current_heading_unwrapped  # Store the unwrapped heading for the next calculation

    #     # Avoid division by zero for both yaw_rate and speed
    #     if yaw_rate == 0 or self.speed == 0:
    #         return float('inf')
        
    #     # Constants for the road's circular curve (center point and radius)
    #     x_r, y_r, Rr = 1600, 1600, self.track_radius  # Assuming these are the center and radius of the road, Rr is the radius of the curved road segment
        
    #     # Radius of the vehicle path Rv as per equation (2)
    #     # Rv = self.speed / abs(yaw_rate) if yaw_rate != 0 else float('inf')

    #     # Calculate the turning radius based on the current steering angle
    #     # Ensure steer is within the allowed range and not zero to avoid division by zero
    #     if self.steer != 0 and -self.max_steer <= self.steer <= self.max_steer:
    #         Rv = self.wheelbase / np.abs(np.tan(self.steer))  # Use absolute value to ensure Rv is always positive
    #     else:
    #         Rv = float('inf')  # Straight line movement, no turning radius

    #     # Calculate the offset (y) from the center of the road and return 0 if oob
    #     offset = self.calculate_offset(x, y)
    #     if offset > 1 or offset < -1:
    #         return 0
        
    #     # Calculate the vehicle curve's center point (Xv, Yv) to the right for a clockwise turn (8)
    #     # Since the car is driving clockwise and turning right, the center of rotation is below the car
    #     x_v = self.pos[0] - Rv * np.sin(self.heading)  # Subtract because sin(self.heading) is negative and we are turning clockwise
    #     y_v = self.pos[1] + Rv * np.cos(self.heading)  # Add because we are moving downwards in the graphical coordinate system

    #     # Compute A' using equation (6) with the offset calculated
    #     # We'll need to use the offset here directly, assuming it's the y distance
    #     A_prime = -1.875 * offset + 1.875

    #     # Compute A using equation (5)
    #     A = Rv - A_prime

    #     # Compute beta_raw as the angle of the vector from the vehicle's turning circle center to the road's circle center
    #     beta_raw = np.arctan2(y_r - y_v, x_r - x_v)

    #     # Normalize beta_raw to the range [0, 2π]
    #     beta_raw = beta_raw % (2 * np.pi)

    #     # Vehicle heading is already normalized to the range [0, 2π]
    #     vehicle_heading = (self.heading + 2 * np.pi) % (2 * np.pi)

    #     # Compute β as the difference between beta_raw and vehicle_heading
    #     # Since β is the angle from the radial line to the vehicle's heading, we need to ensure it's positive
    #     beta = (beta_raw - vehicle_heading + 2 * np.pi) % (2 * np.pi)

    #     # Compute alpha1 ensuring it's within the domain of acos
    #     alpha1_cos_arg = ((A**2) + (Rv**2) - (Rr**2)) / (2 * A * Rv)
    #     alpha1_cos_arg = np.clip(alpha1_cos_arg, -1, 1)  # Clip to the domain of arccos
    #     alpha1 = np.arccos(alpha1_cos_arg)

    #     # Compute alpha ensuring it's the difference between beta and alpha1
    #     alpha = beta - alpha1

    #     # If alpha is negative, it means that beta is smaller than alpha1, which should not be the case
    #     # We add 2π to beta and recompute alpha to ensure it's positive
    #     if alpha < 0:
    #         beta += 2 * np.pi
    #         alpha = beta - alpha1

    #     # Compute DLC using equation (3)
    #     DLC = alpha * Rv

    #     # Compute TLC using equation (1)
    #     TLC = DLC / self.speed if self.speed != 0 else float('inf')

    #     # Out-of-Bounds Check (Optional: This is a simplified check)
    #     max_TLC = 70  # Define this as per your simulation's requirement
    #     TLC = min(TLC, max_TLC)

    #     return TLC