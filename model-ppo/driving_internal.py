from gym.spaces import Discrete, Dict, Box
import numpy as np

import copy
import random
import math

# PART 2/3: THE INTERNAL
# Implementation of an internal (cognitive) environment. 
# Used to simulate the driver's beliefs about the environment, 
# particularly when the driver is not paying attention. 
# The class interacts with the external environment and provides rewards and state updates for an agent.
class driving_internal():

    # Initializes the internal model and sets up some basic parameters, 
    # such as sight span, done area, speed limit, and initial attention state.
    def __init__(self, ext_env, params = None):
       self.ext_env = ext_env # The external environment which the driver actually sees

       # Waypoints are rectangles for ext_env pos, where the first
       # time arriving in this waypoint triggers a positive reward. NOT REALLY NEEDED.
       self.waypoints = []
       self.waypoint_flags = [] # Ones for unvisited waypoints
       self.waypoints_visited = 0 # How many waypoints visited

       self.sight_span = 10 * self.ext_env.resolution_scale

       # Where the task ends
       self.done_area = None

       self.max_time = None
       self.has_attention = True
       self.occlusion = True

       # Variables to track steering and occlusion issues
       self.max_steer_count = 0  # Counts how long the agent has been steering maximally
       self.occlusion_time = 0.0  # Track how long the agent stays occluded
       self.max_occlusion_time = 10.0  # Maximum allowed occlusion time in seconds
       # How many seconds can one episode run.
       self.max_episode_time = 80 # basically same as 'until' in the simulate, but inside the step function

       # How much inattention gets reward each tick
       self.inattention_reward = 0.05 # Best single variable to regulate the occlusion duration

       # Train driving under inattention
       self.training = False # if True agent has been trained with eye-cam
       # When training, use this to close / open eyes
       self.attention_switch_prob = 0.05 # With each step there's 5% change for closing eyes
                                         # Occlusion time depends on speed. NOT IN USE.

       self.speed_limit = [0, 120] # Lower and upper speed limit. Changed suitable for the experiments (60-120km/h) (not needed in all experiments)
                                   # delete speed from action space if not necessary! CHANGE THIS !

       self.reset()

    # Resets the internal model, including the driver's attention state, inattention time, 
    # and the initial beliefs about the external environment. These attributes don't have
    # to be included in the init-function. 
    def reset(self, reset_ext_env = True):
        if reset_ext_env:
            self.ext_env.reset()
        
        self.max_steer_count = 0  # Reset steering count
        self.occlusion_time = 0.0  # Reset occlusion time
        
        self.has_attention = True
        self.prev_attention_state = True # For the update_attention_state function
        self.inattention_time = 0
        self.ext_env.speed = self.ext_env.min_speed
        self.waypoints_visited = 0 # If we don't reset then every time a waypoint is reached in the trained episodes, 
                                   # reward would be based on the total number of waypoints visited across all episodes.
        self.waypoint_flags = []
        for i in range(len(self.waypoints)):
            self.waypoint_flags.append(1)

        # Dictionary to store driver's beliefs about the external environment, used for internally
        # simulating driving during inattention.
        self.obs_belief = {}
        self.obs_belief['pos'] = copy.copy(self.ext_env.pos) # The copy.copy() function is used to create a shallow copy of the position (pos). 
                                                             # This is done to ensure that any changes made to self.obs_belief['pos'] do not affect 
                                                             # the original self.ext_env.pos object. 
        self.obs_belief['front_wheel'] = self.ext_env.front_wheel
        self.obs_belief['back_wheel'] = self.ext_env.back_wheel
        self.obs_belief['heading'] = self.ext_env.heading

        self.total_lane_deviations = 0 # New variable to count the number of times the car goes off-road with or without attention
        self.lane_deviations = 0 # New variable to count the number of times the car goes visibly off-road with attention
        self.occ_lane_deviations = 0 # Variable to keep track how many lane deviations started without attention
        self.car_off_road = False # New variable to indicate if the car is currently off-road
        self.attention_loss_lane_deviation_flag = False  # New flag to track lane deviations during attention loss

        return self.get_belief()
    
    # Track and update the attention state of the agent, specifically for the purpose of counting occlusions and measuring their duration.
    def update_attention_state(self, attention_state):
        if attention_state != self.prev_attention_state: # If the current attention state differs from the previous attention state 
            if not attention_state:  # When lost attention
                self.ext_env.occlusion_start_timestep = self.ext_env.timestep_count # Iniates the occlusion starting point (timestep #number)
            else:  # When regained attention
                occlusion_duration_timesteps = self.ext_env.timestep_count - self.ext_env.occlusion_start_timestep # Counts how many timesteps the occlusion lasted
                self.ext_env.total_occlusion_duration += occlusion_duration_timesteps * self.ext_env.dt # Transforms the duration into secods
                self.ext_env.total_occlusions += 1 # Increments the total occlusion number
        self.prev_attention_state = attention_state # Updates the previous attention state based on the current
        self.ext_env.timestep_count += 1 # Increments the timestep count

    # Function to generate Gaussian noise. When the agent is inattentive, we introduce Gaussian noise to the position and heading updates. 
    # The standard deviation of the noise increases with the inattention time, making the agent's beliefs more uncertain as it spends more 
    # time without attention. Adjust the noise_std_dev scaling factor (0.0005 in the example) to control the rate at which the noise increases. 
    # A larger value will make the agent's beliefs become uncertain more quickly, while a smaller value will result in a slower increase in uncertainty.
    def gaussian_noise(self, mean, std_dev):
        return np.random.normal(mean, std_dev)

    # Updates the driver's beliefs about the car's position, front and back wheel positions, heading, and other 
    # variables based on the driver's attention state. The beliefs are then normalized and returned as a dictionary.
    def get_belief(self):

        # Update the inattention_time depending on the driver's attention state. Inattention time is time since eyes closed,
        # divided by 10 to scale it [0,1] (i.e., assuming there's never over 10s inattention).
        if self.has_attention:
            self.inattention_time = 0
        else:
            if self.inattention_time == 0:
                self.last_seen_pos = copy.deepcopy(self.ext_env.pos)
            self.inattention_time += self.ext_env.dt

        # If the driver has attention...
        if self.has_attention:
            # Fully observable

            # x = self.ext_env.pos[0]
            # y = self.ext_env.pos[1]
            # xmin = x - self.sight_span
            # ymin = y - self.sight_span

            # # Add +1 to include the last row and columns
            # xmax = x + self.sight_span + 1
            # ymax = y + self.sight_span + 1

            # obs = self.ext_env.area[int(xmin):int(xmax),int(ymin):int(ymax)] / self.ext_env.max_area_code

            # ...the beliefs are updated directly from the external environment.
            self.obs_belief['pos'] = copy.copy(self.ext_env.pos)
            self.obs_belief['front_wheel'] = self.ext_env.front_wheel
            self.obs_belief['back_wheel'] = self.ext_env.back_wheel
            self.obs_belief['heading'] = self.ext_env.heading

            # Extract x, y
            x, y = self.ext_env.pos[0], self.ext_env.pos[1]
            # Calculate offset
            offset = self.ext_env.calculate_offset(x=x, y=y)

            # Calculate the time to lane crossing for the current heading (and for slight left and right turns)
            tlc = self.ext_env.tlc  ## Use the real tlc value straight from the environment
        
        else: # If the driver doesn't have attention, beliefs are updated based on the most likely values (without added noise). ENTROPY GROWS WHEN THIS DIFFERS FROM THE ACTUAL (update_pos) POSITION UPDATE, WHICH INTRODUCES NOISE
            pos, f_w, b_w, heading = \
                self.ext_env.simulate_pos_update(self.ext_env.speed,
                                                 self.obs_belief['pos'],
                                                 self.ext_env.steer,
                                                 0, # Introduces steer noise into the internal belief, default = 0
                                                 self.ext_env.max_steer,
                                                 self.obs_belief['heading'],
                                                 self.obs_belief['front_wheel'],
                                                 self.obs_belief['back_wheel'],
                                                 self.ext_env.wheelbase,
                                                 self.ext_env.dt)
            
            # Calculate offset (here the position is based on the beliefs)
            x, y = self.obs_belief['pos'][0], self.obs_belief['pos'][1]
            offset = self.ext_env.calculate_offset(x=x, y=y)

            tlc = self.ext_env.calculate_dynamic_tlc(pos = self.obs_belief['pos']) # calculate tlc using believed position # Calculates the TLC for the car in its current position and heading, based on the internal belief of the agent 
                                                                                   # (self.obs_belief['pos']). This provides the agent with an estimation of how long it would take to cross the 
                                                                                   # lane boundaries if it keeps its current heading and speed. ## TLC UPDATE

            self.obs_belief['pos'] = pos # The updated pos from simulate_pos_update is assigned to the belief dictionary
            self.obs_belief['front_wheel'] = f_w # The updated f_w from simulate_pos_update is assigned to the belief dictionary
            self.obs_belief['back_wheel'] = b_w # The updated b_w from simulate_pos_update is assigned to the belief dictionary
            self.obs_belief['heading'] = heading # The updated heading from simulate_pos_update is assigned to the belief dictionary

        # Normalize TLC and offset with more appropriate scaling (UUSI KOODI)
        tlc_normalized = tlc / 10  # Scale TLC to a maximum of 1 for the neural networks
        offset_normalized = offset  # Keep the offset as is (it already ranges between -1 and 1)

        # Normalize observations and create the final dictionary of beliefs (for neural netwoks, returned as "s" in driving agent)
        self.obs = {#'obs': obs,
            # 'offset': [offset],
            # 'tlc': [tlc/10],
            'offset': [offset_normalized],
            'tlc': [tlc_normalized],
            # 'pos': [self.obs_belief['pos'][0] / self.ext_env.area_bounds,
            #         self.obs_belief['pos'][1] / self.ext_env.area_bounds],
            'inattention': [self.inattention_time / 10],
            'speed': [self.ext_env.speed / self.ext_env.max_speed],
            'heading': [self.obs_belief['heading'] / (2*np.math.pi)],
            'steer': [self.ext_env.steer / np.math.pi]}

        return self.obs
    
    # ORIGINAL Calculates the reward at each time step based on inattention, speed, lane and task completion
    def get_reward(self):
        
        # A speed factor that is directly proportional to speed (e.g., 60 km/h -> 0.6, 100 km/h -> 1)
        speedkmh = self.ext_env.speed * 3.6 # speed in km/h
        speed_factor = speedkmh / 100  # Directly proportional to speed

        reward = 0
        is_done = False
        
        # Checks if the simulation time has exceeded the maximum episode time.
        if self.ext_env.time >= self.max_episode_time:
            reward -= 10 #  If the maximum episode time is exceeded, penalty of 50 is added to r.
            is_done = True # Set the done flag to True.
        
        # -- Insert OOB-time check:
        if self.ext_env.current_oob_time > self.ext_env.max_oob_time:
            reward -= 10  # If the maximum oob time is exceeded, penalty of 10 is added to r.
            is_done = True
        
        # Use actual TLC (time-to-lane crossing) values for reward calculation
        if not self.occlusion:
            reward += self.ext_env.tlc * speed_factor

        # If the driver is inattentive, give a small positive reward based on tlc & speed
        if not self.has_attention:
            reward += self.inattention_reward * self.ext_env.tlc * speed_factor # (inattention reward range: 0.01 - 0.1. NOW 0.05)

         # If the driver is not in the correct lane (area code = 2) during attention, give a negative reward per timestep.
        if self.has_attention and self.ext_env.current_area() != 1: # 1 = on the lane
            reward -= 1 #* abs(self.ext_env.offset)
            
        # Reward based on the number of timesteps with no attention when the agent turns its attention back on
        if self.has_attention and self.ext_env.inattention_counter > 0: # Agent just regained attention
            on_lane = self.ext_env.current_area() == 1
            if on_lane:
                reward += self.ext_env.inattention_counter
                reward += 0.4 * self.ext_env.tlc * speed_factor
            else: 
                reward += -self.ext_env.inattention_counter
            self.ext_env.inattention_counter = 0  # Reset the counter for inattention
            reward -= 1 # Negative reward when ending the occlusion to prevent switching ON/OFF rapidly

        # If the driver is within the done_area (task completion area), give a positive reward.
        if self.within(self.ext_env.pos, self.done_area):
            reward += 60
            is_done = True

        if is_done == True:
            return reward, True
        else:
            return reward, False

    # Returns the value of the current area of the environment that the car is located in directly from the ext_env (external environment) 
    # instance's area attribute, using the car's current position (pos) as coordinates.
    def get_current_area(self):
        area = self.ext_env.area[round(self.ext_env.pos[0])][round(self.ext_env.pos[1])]
        return area

    # The function only returns the index i if that waypoint is flagged as "not visited" (with a value of 1).
    def in_waypoint(self):
        for i in range(len(self.waypoints)):
            if self.waypoint_flags[i] == 1 and self.within(self.ext_env.pos, self.waypoints[i]):
                return i
        return None

    # Checks if the agent's current position is within a certain rectangle
    def within(self, pos, rect): # args include pos of agent and the rectangle area
        return pos[0] >= rect[0] and pos[1] >= rect[1] and pos[0] <= rect[2] and pos[1] <= rect[3]

    def add_waypoint(self, rect):
        self.waypoints.append(rect)
        self.waypoint_flags.append(1)
    
    def get_position(self):
        return self.pos, self.heading

    def step(self, action): # ARRIVES HERE FROM THE AGENT'S STEP (4)
        # Store the action given from the agent into the class.
        self.action = action

        # Scale the first element of the action (acceleration) by the maximum acceleration defined in the external environment 
        action[0] = action[0] * self.ext_env.max_acceleration

        # Scale the second element of the action (steering angle) by the maximum steering angle defined in the external environment.
        action[1] = action[1] * self.ext_env.max_steer

#-------------------------------------------------
        # Update attention based on the action's third element (action[2]). By checking that the counter is equal to 0, 
        # we ensure that the agent can only turn the occlusion off if it is currently on (i.e., has_attention is False)
        if action[2] > 0 and self.occlusion and self.ext_env.attention_counter == 0: # When value > 0, that means attention is TRUE
            self.has_attention = True
        if self.has_attention and self.occlusion:
            self.ext_env.attention_counter += 1
        if self.ext_env.attention_counter > 5:  # 5 time steps for has_attention to be True
            self.has_attention = False
            self.ext_env.attention_counter = 0
#-------------------------------------------------
        # Increments the inattention counter by 1 when occluded
        if not self.has_attention:
            self.ext_env.inattention_counter += 1

        # Execute the action in the external environment. ACTION COMES FROM AGENT AND GIVES TO THE EXT_ENV (5). 
        self.ext_env.step(action)

        # UPDATE LANE DEVIATIONS
        # 1. Lane deviation started while agent has no attention
        if not self.has_attention and self.ext_env.current_area() != 1 and not self.car_off_road:
            self.car_off_road = True
            self.total_lane_deviations += 1
            self.attention_loss_lane_deviation_flag = True  # Set the flag
            self.occ_lane_deviations += 1  # Particular counter for lane deviations that start during an occlusion

        # 2. Lane deviation continues while agent regained attention
        elif self.has_attention and self.ext_env.current_area() != 1 and self.attention_loss_lane_deviation_flag:
            self.lane_deviations += 1
            self.attention_loss_lane_deviation_flag = False  # Clear the flag

        # 3. Lane deviation started while agent has attention
        elif self.has_attention and self.ext_env.current_area() != 1 and not self.car_off_road:
            self.lane_deviations += 1
            self.car_off_road = True
            self.total_lane_deviations += 1

        # 4. Agent returned back on the road
        elif self.ext_env.current_area() == 1:
            self.car_off_road = False
            self.attention_loss_lane_deviation_flag = False

        # RETURN THE UPDATED BELIEF STATE, REWARD AND DONE TO THE AGENT (8). 
        # Return the updated belief state, reward, and done flag
        s = self.get_belief()
        r, d = self.get_reward()  # Handles both reward and done flag
        return s, r, d  # Done flag (d) is passed up to the agent

        ### NOT IN USE REWARDS: ###
    
        #speedkmh = self.ext_env.speed * 3.6
        #speed_factor = max(1, (120 - speedkmh) / 30)  # Scale factor inversely proportional to speed, 120 and 20 are chosen based on the speed range (60 to 120) to normalize the factor
        #penalty_factor = max (1, (speedkmh - 60))

        # # Adjust the reward for inattention dynamically based on speed
        # speed_factor = max(1, (120 - speedkmh) / 40)  # Scale factor inversely proportional to speed, 120 and 50 are chosen based on the speed range (60 to 120) to normalize the factor

        # # New penalty factor for speed, directly proportional to speed
        # penalty_factor = max(0, (speedkmh - 60) / 40)  # Assuming 60 km/h is the minimum speed; larger the speed = larger the penalty (max. 1 if 120 maximum speed)
    
        # # If occlusion = False, tries to center the car on the lane (+++ for inattention maximization)
        # if not self.occlusion:
        #     reward += -0.5 * abs(self.ext_env.offset)
    
        # reward -= 1.5 * (self.ext_env.speed / 16.666)  # Increase the penalty for being out of lane at higher speeds

        #         # If the driver is inattentive, give a small positive reward based on speed
        # if not self.has_attention:
        #     reward += self.inattention_reward # (inattention reward range: 0.01 - 0.1. NOW 0.05)

        # # Continuous steering stability reward
        # reward -= 0.5 * abs(self.ext_env.steer - self.ext_env.prev_steer)

        # # Continuous negative reward based on TLC when attentive
        # if self.has_attention:
        #     reward -= 0.5 * (1 - self.ext_env.tlc / 10) # max tlc of 10 giving no negative reward at all
    
        # # If the agent just gained attention, give reward based on TLC threshold
        # if self.has_attention and self.ext_env.inattention_counter > 0 and self.ext_env.tlc < 5:
        #     reward -= self.ext_env.tlc
        # else:
        #     reward += self.ext_env.tlc
            
        # # Waypoint rewards for agent to understand the assignment of driving straight
        # i = self.in_waypoint() # i gets different value if in waypoint
        # if i != None: # check if the waypoint hasn't been visited yet
        #    self.waypoints_visited += 1  # increase the count of visited waypoints
        #    self.waypoint_flags[i] = 0 # mark that waypoint as visited            
        #    reward += 5 * self.waypoints_visited # give an increasing reward based on the number of visited waypoints
    
        # # While attention OFF, apply a 10% offset-related sanction if the agent deviates from the lane.
        # if not self.has_attention:
        #     if self.ext_env.offset >= 1 or self.ext_env.offset <= -1:
        #         offset_sanction = -0.1 * abs(self.ext_env.offset)
        #         reward += offset_sanction
    
        # If the driver is inattentive, give a small positive reward (logarithmic, initial inattention reward = 0.1)
        # if self.has_attention == False:
        #     reward += self.inattention_reward * (math.log(self.ext_env.inattention_counter + 1))
        
        # # If the current speed of the vehicle is below the lower speed limit
        # if self.ext_env.speed < self.speed_limit[0]:
        #     # Compute the difference between the lower speed limit and the current speed
        #     speed_diff = self.speed_limit[0] - self.ext_env.speed
        #     # Subtract from the reward a penalty proportional to the square of the speed difference. 
        #     # Squaring the difference means the penalty increases rapidly for larger differences.
        #     # Dividing by 10 reduces the magnitude of the penalty.
        #     reward -= (speed_diff ** 2) / 20

        # # If the current speed of the vehicle is above the upper speed limit
        # if self.ext_env.speed > self.speed_limit[1]:
        #     # Compute the difference between the current speed and the upper speed limit
        #     speed_diff = self.ext_env.speed - self.speed_limit[1]
        #     # Similar to the above case, subtract from the reward a penalty proportional 
        #     # to the square of the speed difference, and reduce the magnitude by dividing by 10
        #     reward -= (speed_diff ** 2) / 20
        
        # If the agent is occluded and steers to max angle, give a small negative reward to avoid spinning.
        # if not self.has_attention and abs(self.ext_env.steer) >= abs(self.ext_env.max_steer) - 0.001: # Substraction because of floating point precision
        #     reward -= 0.5

        # # Discourage Rapid Toggling
        # if self.has_attention and self.ext_env.inattention_counter > 0: 
        #     penalty_factor = 1 / (self.ext_env.inattention_counter + 1)
        #     reward -= penalty_factor
            
            # if self.ext_env.inattention_counter < 15: # If the agent regained attention early (shorter than 1.5s occlusions)
            #     reward -= 0.5  # Penalty for regaining attention too soon
            # elif self.ext_env.inattention_counter > 50: # If the agent keeps occlusion on for too long (longer than 5s occlusions)
            #     reward -= 1.0  # Increased penalty for regaining attention after longer time
            # else:
            #     reward += 0.0  # Give zero reward for regaining attention after a "optimal" amount of time
        
        # # Reward (sanction) based on offset right after regaining attention right after occlusion
        # if self.has_attention and self.ext_env.inattention_counter > 0: # Agent just regained attention
        #     # Apply offset reward only if offset is outside of -1 and 1 range.
        #     if self.offset > 1 or self.offset < -1:
        #         offset_reward = -1 * abs(self.offset)
        #     else:
        #         offset_reward = 0
        #     reward += offset_reward
        #     self.ext_env.inattention_counter = 0  # Reset the counter after giving the reward
        # elif self.has_attention:  # Agent continues to have attention
        #     # Apply a smaller offset-related sanction if the agent deviates from the lane.
        #     if self.offset > 1 or self.offset < -1:
        #         offset_sanction = -0.1 * abs(self.offset)
        #         reward += offset_sanction

            # # Calculates the reward at each time step based on inattention, speed, lane and task completion
    # def get_reward(self):

    #     speedkmh = self.ext_env.speed * 3.6 # speed in km/h
    #     # A speed factor that is directly proportional to speed (e.g., 60 km/h -> 0.6, 100 km/h -> 1)
    #     speed_factor = (speedkmh / 100)  # Directly proportional to speed

    #     on_lane = self.ext_env.current_area() == 1

    #     reward = 0
        
    #     # Use actual TLC (time-to-lane crossing) values for reward calculation
    #     if not self.occlusion:
    #         reward += self.ext_env.tlc * speed_factor # Rewardi sidottu ajotehtävän vaativuuteen

    #     # If the driver is not in the correct lane (area code = 2) during attention, give a negative reward per timestep.
    #     if self.has_attention and not on_lane:
    #         #reward -= abs(self.ext_env.offset)
    #         reward -= 1
        
    #     # If the driver is inattentive, give a small positive reward based on speed
    #     if not self.has_attention:
    #         reward += self.inattention_reward # (inattention reward range: 0.01 - 0.1. NOW 0.1)
            
    #     # Reward based on the number of timesteps with no attention when the agent turns its attention back on
    #     if self.has_attention and self.ext_env.inattention_counter > 0: # Agent just regained attention
    #         reward -= 1.5 / speed_factor # Negative reward (tied to speed) when ending the occlusion to prevent switching ON/OFF rapidly

    #         ## Speed factor low käytännössä inverse of high eli (1/speed_factor_high) >>> selitä paperissa näin 

    #         if on_lane:
    #             reward += self.ext_env.inattention_counter
    #             reward += 0.75 * self.ext_env.tlc * speed_factor
    #         else: 
    #             reward += -self.ext_env.inattention_counter
    #         self.ext_env.inattention_counter = 0  # Reset the counter for inattention
        
    #     reward = reward * speed_factor

    #     # If the driver is within the done_area (task completion area), give a positive reward.
    #     if self.within(self.ext_env.pos, self.done_area):
    #         reward += 60

    #     return reward
    
    # # Calculates the reward at each time step based on inattention, speed, lane and task completion
    # def get_reward(self):

    #     reward = 0
        
    #     # Use actual TLC (time-to-lane crossing) values for reward calculation
    #     if not self.occlusion:
    #         reward += self.ext_env.tlc

    #     # If the driver is inattentive, give a small positive reward based on speed
    #     if not self.has_attention:
    #         reward += self.inattention_reward #* speed_factor # (inattention reward range: 0.01 - 0.1. NOW 0.1)

    #      # If the driver is not in the correct lane (area code = 2) during attention, give a negative reward per timestep.
    #     if self.has_attention and self.ext_env.current_area() != 1: # 1 = on the lane
    #         reward -= 1
            
    #     # Reward based on the number of timesteps with no attention when the agent turns its attention back on
    #     if self.has_attention and self.ext_env.inattention_counter > 0: # Agent just regained attention
    #         on_lane = self.ext_env.current_area() == 1
    #         if on_lane:
    #             reward += self.ext_env.inattention_counter
    #             reward += 0.5 * self.ext_env.tlc
    #         else: 
    #             reward += -self.ext_env.inattention_counter
    #         self.ext_env.inattention_counter = 0  # Reset the counter for inattention
    #         reward -= 1 # Negative reward when ending the occlusion to prevent switching ON/OFF rapidly

    #     # If the driver is within the done_area (task completion area), give a positive reward.
    #     if self.within(self.ext_env.pos, self.done_area):
    #         reward += 60

    #     return reward

       # # THE NEW Calculates the reward at each time step based on inattention, speed, lane and task completion
    # def get_reward(self):

    #     is_done = False
        
    #     # Speed transformed to kmh
    #     speedkmh = self.ext_env.speed * 3.6
    #     # A multiplier directly proportional to speed
    #     speed_factor = max(1, (speedkmh - 40) / 25)  # Larger the speed = larger the multiplier

    #     reward = 0

    #     ####### TERMINATE IF MAX EPISODE TIME #######
    #     if self.ext_env.time >= self.max_episode_time: # Checks if the simulation time has exceeded the maximum episode time.
    #         reward -= 10 #  If the maximum episode time is exceeded, penalty of 10 is added to r.
    #         is_done = True # Set the done flag to True.
        
    #             ####### OOB PENALTY #######
    #     # Track out-of-bounds (OOB) time
    #     if self.ext_env.current_area() != 1:  # Check if the agent is outside the road
    #         self.ext_env.current_oob_time += self.ext_env.dt  # Increment OOB time if agent is off-road
    #         if self.ext_env.current_oob_time > self.ext_env.max_oob_time:  # Check if OOB time exceeds allowed maximum
    #             #reward -= abs(self.ext_env.offset) * 10  # Apply penalty based on how far off the road the agent is
    #             is_done = True  # done flag True due to exceeding OOB time 
    #     else:
    #         self.ext_env.current_oob_time = 0  # Reset OOB time when back on the road
        
    #     # Use actual TLC (time-to-lane crossing) values for reward calculation
    #     if not self.occlusion:
    #         reward += self.ext_env.tlc * speed_factor # Rewardi sidottu ajotehtävän vaativuuteen

    #     # If the driver is not in the correct lane (area code = 2) during attention, give a negative reward per timestep.
    #     if self.has_attention and self.ext_env.current_area() != 1: # 1 = on the lane
    #         reward -= 1
            
    #     # Reward based on the number of timesteps with no attention when the agent turns its attention back on
    #     if self.has_attention and self.ext_env.inattention_counter > 0: # Agent just regained attention
    #         reward -= 3 * 1/speed_factor # Negative reward tied to speed when ending the occlusion to prevent switching ON/OFF rapidly

    #         ## Speed factor low käytännössä inverse of high eli (1/speed_factor_high) >>> selitä paperissa näin 

    #         on_lane = self.ext_env.current_area() == 1
    #         if on_lane:
    #             reward += self.ext_env.inattention_counter
    #             reward += 0.5 * self.ext_env.tlc * speed_factor
    #         else: 
    #             reward += -self.ext_env.inattention_counter
    #         self.ext_env.inattention_counter = 0  # Reset the counter for inattention

    #             ####### TASK COMPLETION BONUS (SCALED BY SPEED) #######

    #     # Task completion bonus, encouraging faster task completion without sacrificing safety
    #     if self.within(self.ext_env.pos, self.done_area):
    #         completion_bonus = 60
    #         reward += completion_bonus
    #         is_done = True # Terminates the episode due to arriving to goal

    #     ####### NORMALIZE REWARD BY EPISODE LENGTH #######
    #     reward *= speed_factor  # Normalize by speed factor to balance episode duration discrepancies

    #     if is_done: 
    #         return reward, True # Terminate the episode
    #     else:
    #         return reward, False  # No termination, continue the episode

    # # NEWEST Calculates the reward at each time step based on inattention, speed, lane and task completion etc.
    # def get_reward(self):

    #     reward = 0
    #     is_done = False

    #     speedkmh = self.ext_env.speed * 3.6 # speed in km/h
    #     # A speed factor that is directly proportional to speed (e.g., 60 km/h -> 0.6, 100 km/h -> 1)
    #     speed_factor = speedkmh / 100  # Directly proportional to speed

    #     ####### TERMINATE IF MAX EPISODE TIME #######
    #     if self.ext_env.time >= self.max_episode_time: # Checks if the simulation time has exceeded the maximum episode time.
    #         reward -= 50 #  If the maximum episode time is exceeded, penalty of 10 is added to r.
    #         is_done = True # Set the done flag to True.

    #     ####### OOB PENALTY #######
    #     # Track out-of-bounds (OOB) time
    #     if self.ext_env.current_area() != 1:  # Check if the agent is outside the road
    #         self.ext_env.current_oob_time += self.ext_env.dt  # Increment OOB time if agent is off-road
    #         if self.ext_env.current_oob_time > self.ext_env.max_oob_time:  # Check if OOB time exceeds allowed maximum
    #             reward -= abs(self.ext_env.offset) * 10  # Apply penalty based on how far off the road the agent is
    #             is_done = True  # done flag True due to exceeding OOB time 
    #     else:
    #         self.ext_env.current_oob_time = 0  # Reset OOB time when back on the road

    #     ####### LANE DEVIATION PENALTY #######
    #     # Penalty for lane deviation during attention
    #     if self.has_attention and self.ext_env.current_area() != 1:  # 1 = on the lane
    #         reward -= 1 * abs(self.ext_env.offset)

    #     ####### INITIAL TLC REWARD (WITHOUT OCCLUSIONS INTRODUCED) #######
    #     # Use actual TLC (time-to-lane crossing) values for reward calculation
    #     if not self.occlusion:
    #         reward += self.ext_env.tlc * speed_factor # Reward tied to the speed 

    #     ####### INATTENTION REWARD #######
    #     # Reward for occlusion, but no longer scaled by speed to avoid longer occlusions at higher speeds
    #     if not self.has_attention:
    #         reward += self.inattention_reward  # Minimal occlusion inattention reward

    #     ####### TLC REWARD (OCCLUSIONS INTRODUCED) #######
    #     # Reward when occlusion ends (based on TLC and inattention counter)
    #     if self.has_attention and self.ext_env.inattention_counter > 0:
    #         on_lane = self.ext_env.current_area() == 1
    #         if on_lane:
    #             # Add the speed-adjusted TLC reward
    #             reward += self.ext_env.inattention_counter
    #             reward += 0.5 * self.ext_env.tlc * speed_factor
    #         else:
    #             reward -= self.ext_env.inattention_counter  # Penalty for lane deviation during occlusion

    #         self.ext_env.inattention_counter = 0  # Reset occlusion counter
    #         reward -= 2 / speed_factor # Minor penalty to prevent frequent occlusion switching

    #     ####### EXCESSIVE STEERING PENALTY #######
    #     # Penalty for excessive steering
    #     if abs(self.ext_env.steer) >= self.ext_env.max_steer - 0.01:  # Near max steer
    #         self.max_steer_count += 1
    #     else:
    #         self.max_steer_count = 0  # Reset if not max steering

    #     if self.max_steer_count >= 10:  # If max steer for 10 timesteps (1 second)
    #         reward -= 10  # Apply penalty for excessive steering
    #         is_done = True # done flag True due to the excessive max steering

    #     if self.occlusion_time >= self.max_occlusion_time:
    #         reward -= 10  # Apply penalty for prolonged occlusion
    #         is_done = True  # done flag True due to excessive occlusion

    #     ####### TASK COMPLETION BONUS (SCALED BY SPEED) #######
    #     # Task completion bonus, encouraging faster task completion without sacrificing safety
    #     if self.within(self.ext_env.pos, self.done_area):
    #         completion_bonus = 100
    #         reward += completion_bonus
    #         is_done = True # Terminates the episode due to arriving to goal

    #     ####### NORMALIZE REWARD BY EPISODE LENGTH #######
    #     reward *= speed_factor  # Normalize by speed factor to balance episode duration discrepancies

    #     if is_done: 
    #         return reward, True # Terminate the episode
    #     else:
    #         return reward, False  # No termination, continue the episode

    # def get_reward(self):
    #     # Speed transformed to km/h
    #     speedkmh = self.ext_env.speed * 3.6
    #     # A multiplier inversely proportional to speed
    #     speed_factor = max(1, (120 - speedkmh) / 25)  # Scale inversely proportional to speed

    #     on_lane = self.ext_env.current_area() == 1  # Check if agent is on the lane
    #     reward = 0
    #     is_done = False  # Tracks if the episode is complete

    #     # Termination condition if simulation time exceeds maximum episode time
    #     if self.ext_env.time >= self.max_episode_time:
    #         reward -= 50  # Penalty for exceeding max time
    #         is_done = True

    #     # Reward based on actual TLC (time-to-lane crossing) values
    #     if not self.occlusion:
    #         reward += self.ext_env.tlc * (1 / speed_factor)

    #     # Penalty for being off-lane during attention
    #     if self.has_attention and not on_lane:
    #         reward -= abs(self.ext_env.offset)  # Penalty based on offset from the lane

    #     # Reward during inattention based on speed
    #     if not self.has_attention:
    #         reward += self.inattention_reward  # Reward to encourage optimal occlusion use

    #     # Penalty and reward for regaining attention after inattention
    #     if self.has_attention and self.ext_env.inattention_counter > 0:  # Just regained attention
    #         reward -= 1 * speed_factor  # Negative reward to discourage rapid attention toggling

    #         if on_lane:
    #             reward += self.ext_env.inattention_counter  # Reward for inattention steps
    #             reward += 0.5 * self.ext_env.tlc * (1 / speed_factor)
    #         else:
    #             reward -= self.ext_env.inattention_counter  # Penalty for being off-lane

    #         self.ext_env.inattention_counter = 0  # Reset counter

    #     # Penalty for exceeding off-road time
    #     if self.has_attention and not on_lane:
    #         self.ext_env.current_oob_time += self.ext_env.dt
    #         if self.ext_env.current_oob_time > self.ext_env.max_oob_time:
    #             reward -= 20  # Additional penalty for excessive OOB time
    #             is_done = True
    #     else:
    #         self.ext_env.current_oob_time = 0  # Reset OOB time when back on lane

    #     # Positive reward for task completion
    #     if on_lane and self.within(self.ext_env.pos, self.done_area):
    #         reward += 60
    #         is_done = True

    #     if is_done:
    #         return reward, True
    #     else:
    #         return reward, False
