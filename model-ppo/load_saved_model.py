# Load the trained and saved model.
import os
from stable_baselines3 import PPO

# Import necessary modules and classes
from driving_environment import driving_environment
from driving_internal import driving_internal
from driving_agent import driving_agent
from map_generator import make_circular_road
from animate_trace import animate_trace

# Iniate the environment, load the saved model, simulate and draw a trace
if __name__ == "__main__":

    # Setting up the environment and agent similar to your debug code
    d_env = driving_environment(area_bounds = 1650 * 1)
    d_env.start_heading = -1.08 # equals 5.2 RADIANS
    d_env.start_pos = [328.5 * d_env.resolution_scale, 804.5 * d_env.resolution_scale] # ~1km until done area
    d_env.steer_noise = 0.003 # Steering wheel related constant noise; should be kept same for everyone. #CHANGE THIS
    #d_env.max_steer = 0.0035
    #d_env.max_acceleration = 0.5

    # d_env = driving_environment(area_bounds = 1650)
    # d_env.start_heading = -0.26
    # d_env.start_pos = [1224.99, 146.25]
    # d_env.steer_noise = 0.000
    # d_env.steer_action_noise = 0.000
    # d_env.max_steer = 1
    # d_env.max_acceleration = 1 

    make_circular_road(d_env, 1600 * d_env.resolution_scale, 1600 * d_env.resolution_scale, d_env.track_radius, d_env.track_width) ##TO CHANGE E.G. WIDTH, MODIFY IN driving_environment.py !!

    d_int = driving_internal(d_env)
    d_int.reset()
    d_int.add_waypoint([380 * d_env.resolution_scale, 723 * d_env.resolution_scale, 385 * d_env.resolution_scale, 725 * d_env.resolution_scale])
    d_int.add_waypoint([530 * d_env.resolution_scale, 545 * d_env.resolution_scale, 535 * d_env.resolution_scale, 547 * d_env.resolution_scale])
    d_int.add_waypoint([795 * d_env.resolution_scale, 331 * d_env.resolution_scale, 800 * d_env.resolution_scale, 333 * d_env.resolution_scale])
    d_int.done_area = [1091 * d_env.resolution_scale, 184 * d_env.resolution_scale, 1098 * d_env.resolution_scale, 191 * d_env.resolution_scale]

    d_int.occlusion = True

    a = 16.666
    b = 27.777

    speed = d_env.resolution_scale * a # Insert speed here (a = 60 km/h or b = 100 km/h) #CHANGE THIS
    d_env.max_speed = speed # Max speed of the agent
    d_env.min_speed = speed # Min speed of the agent
    
    d_agent = driving_agent(d_int)

    # Set the path to your saved model
    model_path = "/Users/tonialha/Documents/CODEFOLDER/MODEL_DATA/SAVED_MODELS/MODEL_EXP1_0.003.pkl" #CHANGE THIS

    # Load the saved model into the agent's model attribute
    try:
        d_agent.agent = PPO.load(model_path, env=d_agent)
        print(f"Model loaded successfully from: {model_path}")
    except Exception as e:
        print(f"Error loading model: {e}")
        exit(1)
    
    first_layer_weights = list(d_agent.agent.policy.state_dict().values())[0]
    print(f"First layer weight shape: {first_layer_weights.shape}")

################ CONTINUE TRAINING AND SAVE MODEL #################
    
    # # CONTINUE TRAINING WITH THE CURRICULUM
    # d_agent.train_curriculum()  # This will use the loaded model for curriculum training

    # # Save the continued model to a new path
    # new_save_path = "/Users/tonialha/Documents/CODEFOLDER/MODEL_DATA/SAVED_MODELS/model_x.pkl"
    # d_agent.agent.save(new_save_path)
    # print(f"CONTINUED MODEL SAVED TO {new_save_path}")

####################################################################

    # SIMULATION
    d_env.max_oob_time = 5
    print(d_agent.simulate(get_trace=True, deterministic=True))
    d_agent.int_env.ext_env.plot_trace(full_area=True, waypoints=d_agent.int_env.waypoints, done_area=d_agent.int_env.done_area)
    animate_trace(d_agent.int_env.ext_env, d_agent.trace)

    # WITH NOISE = 0.002 & 0.005, INATTENTION R = 0.05 (0.1 for 60km/h), TLC 0.5, R1&R2 = 1, TIMESTEPS 1,500,000.