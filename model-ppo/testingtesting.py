# HOW TO RUN: 
# 1. Check your terminal is in the correct virtual environment (3.9.6 'my_venv')
# 2. Also check that (my_venv) tonialha@tonialha-MacBook-Pro-WHFFG6GD4J CODEFOLDER %
# 3. Write python3 /model-ppo/testingtesting.py (or wherever the file is located in CODEFOLDER)
# 4. Enter and RUN !

import os

model_path = "/Users/tonialha/Documents/CODEFOLDER/MODEL_DATA/SAVED_MODELS/model_x.pkl"

# Check if file exists
if os.path.exists(model_path):
    print("Model file found!")
else:
    print("Model file NOT found!")

# Check if a .zip version exists
zip_model_path = model_path + ".zip"
if os.path.exists(zip_model_path):
    print("Model .zip file found!")
else:
    print("Model .zip file NOT found!")
